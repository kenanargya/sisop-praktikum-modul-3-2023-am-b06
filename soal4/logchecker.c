//Glenaya (5025211202)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#define _GNU_SOURCE

int main() {
    // mengambil kata kunci "ACCESSED" dari log.txt dan dihitung
    printf ("\e[33mTotal banyak akses:\e[0m\n");
    system("grep ACCESSED log.txt | wc -l");

    // mendapatkan list seluruh nama folder yang dibuat
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \| awk -F 'categorized' '{print \"categorized\"$NF}' \
    | sed '/^$/d' \| sort \| uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");


    // total file tiap extension terurut secara ascending
    printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \ | grep -o 'categorized/[^o]*' \ | sed 's/categorized\\///' \
    | sed '/^$/d' \ | sed 's/ \\([^o]*\\)//' \ | sort \| uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");

    return 0;
}

