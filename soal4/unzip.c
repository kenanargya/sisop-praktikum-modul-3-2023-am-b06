#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <errno.h>
#include <time.h>
#include <sys/stat.h>
#define _GNU_SOURCE

void download_file(char downloadLink[], char outputName[]) {
    if (fork() == 0) {
        char *arguments[] = {"wget", downloadLink, "-O", outputName, NULL};
        execv("/bin/wget", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0);
    return;
}
void unzip(char zipName[]) {
    if (fork() == 0) {
        char *arguments[] = {"unzip", zipName, NULL};
        execv("/usr/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0);
    return;
}

int main () {
	download_file("https://docs.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp","hehe.zip");
	unzip("hehe.zip");
}
