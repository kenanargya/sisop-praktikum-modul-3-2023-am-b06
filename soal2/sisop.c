#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL2 5
#define SHM_SIZE sizeof(int[ROW1][COL2])

unsigned long long Factorial(int n) {
    unsigned long long ans = 1;
    for (int i = 1; i <= n; i++) {
        ans *= i;
    }
    return ans;
}

int main() {
    // Mengakses shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    // Menghubungkan shared memory ke variabel hasil
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);
    if (shared_ans == (int (*)[COL2]) -1) {
        perror("shmat");
        return 1;
    }

    printf("Matriks hasil perkalian:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d\t", shared_ans[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            // Menghitung faktorial dari setiap elemen matriks
            unsigned long long ans = Factorial(shared_ans[i][j]);
            
            printf("%llu\t", ans);
        }
        printf("\n");
    }

    shmdt(shared_ans);

    return 0;
}
