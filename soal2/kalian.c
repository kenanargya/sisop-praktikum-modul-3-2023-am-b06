#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
#define SHM_SIZE sizeof(int[ROW1][COL2])

int main() {
    int matriks1[ROW1][COL1], matriks2[ROW2][COL2], ans[ROW1][COL2];
    int i, j, k;

    srand(time(NULL)); // inisialisasi seed untuk random number generator

    // mengisi matriks1 dengan angka random dari 1-5 secara inklusif
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            matriks1[i][j] = rand() % 5 + 1;
        }
    }

    // mengisi matriks2 dengan angka random dari 1-4 secara inklusif
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            matriks2[i][j] = rand() % 4 + 1;
        }
    }

    // mengalikan kedua matriks
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            ans[i][j] = 0;
            for (k = 0; k < COL1; k++) {
                ans[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }

    // mengalokasikan shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    // menghubungkan shared memory ke variabel hasil
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);
    if (shared_ans == (int (*)[COL2]) -1) {
        perror("shmat");
        return 1;
    }

    // menyalin hasil perkalian matriks ke shared memory
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            shared_ans[i][j] = ans[i][j];
        }
    }
    
    // menampilkan matriks hasil perkalian
    printf("Hasil perkalian matriks:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d\t", ans[i][j]);
        }
        printf("\n");
    }

    shmdt(shared_ans);

    printf("Matriks hasil perkalian tersimpan dalam shared memory.\n");

    return 0;
}
