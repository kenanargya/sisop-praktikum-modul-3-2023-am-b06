#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL2 5
#define SHM_SIZE sizeof(int[ROW1][COL2])

typedef struct {
    int rows;
    int cols;
} ThreadData;

unsigned long long Factorial(int n) {
    unsigned long long ans = 1;
    for (int i = 1; i <= n; i++) {
        ans *= i;
    }
    return ans;
}

void *FactorialCount(void *arg) {
    ThreadData *data = (ThreadData *)arg;
    int rows = data->rows;
    int cols = data->cols;

    // Mengambil nilai dari shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);

    // Menghitung faktorial
    unsigned long long ans = Factorial(shared_ans[rows][cols]);

    printf("%llu\t", ans);

    shmdt(shared_ans);

    free(data);
    pthread_exit(NULL);
}

int main() {
    // Mengakses shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    // Menghubungkan shared memory ke variabel hasil
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);
    if (shared_ans == (int (*)[COL2]) -1) {
        perror("shmat");
        return 1;
    }

    printf("Matriks hasil perkalian:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d\t", shared_ans[i][j]);
        }
        printf("\n");
    }

    // Membuat thread untuk setiap elemen matriks
    pthread_t threads[ROW1 * COL2];
    int threadIndex = 0;

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            ThreadData *data = malloc(sizeof(ThreadData));
            data->rows = i;
            data->cols = j;

            // Membuat thread
            if (pthread_create(&threads[threadIndex], NULL, FactorialCount, (void *)data) != 0) {
                fprintf(stderr, "Error creating thread\n");
                return 1;
            }
            pthread_join(threads[i*j], NULL);
            threadIndex++;
        }
        printf("\n");
    }

    // Menunggu semua thread selesai
   // for (int i = 0; i < ROW1 * COL2; i++) {
        
    //}

    shmdt(shared_ans);

    return 0;
}
