# sisop-praktikum-modul-3-2023-AM-B06

Anggota kelompok B06 adalah :

|        Nama           | NRP|
| ---                   |--- |
|Glenaya                |5025211202 |
|Gracetriana Survinta  Septinaputri | 5025211199 |
|Ken Anargya Alkausar   | 5025211168 |


# Laporan Resmi Praktikum Modul 3 Sisop

## Soal 1
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut!

### Penyelesaian 1A
>- Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
```c
for (int i=0; i<sizeof(freq)/sizeof(freq[0]); i++) {
        	buffer[con++] = freq[i];
    	}

		write(fd1[1], buffer, con);
		close(fd1[1]);
		wait(NULL); 

		printf("pipe 2 telah berhasil\n");

		close(fd2[1]);
		char Compreceive[4096];

		read(fd2[0], Compreceive, sizeof(Compreceive));
		close(fd2[0]);
		printf("\nencoded: \n%s\n", Compreceive);
		printf("\ndecoded: \n%s\n", copy);

		printf("\nTotal bit SEBELUM dilakukan kompresi huffman : %lu\n", strlen(copy)*8);
		printf("Total bit SETELAH dilakukan kompresi huffman : %lu\n", strlen(Compreceive));

	}else if(cid==0){	///child
		close(fd1[1]);
		char bufferc[100];
		char arrc[ALF];
		int freqc[ALF];
		int conc=0;

		read(fd1[0], bufferc, sizeof(bufferc));
		close(fd1[0]);

		for (int i=0; i<sizeof(arrc); i++) {
        	arrc[i]=bufferc[conc++];
    	}
```
Pada proses induk, terdapat loop for yang membaca nilai dari array freq dan menuliskannya ke dalam buffer, kemudian buffer tersebut dituliskan ke dalam pipe fd1[1]. Setelah itu, pipe fd1[1] ditutup dan proses induk menunggu sampai proses anak selesai dieksekusi menggunakan fungsi wait(NULL). Selanjutnya, proses induk membaca data dari pipe fd2[0] ke dalam array Compreceive, kemudian menutup pipe fd2[0] dan mencetak data yang telah dikompresi (Compreceive) dan data asli (copy) pada layar. Terakhir, program mencetak total bit sebelum dan sesudah kompresi Huffman pada layar.

Pada proses anak, pertama-tama pipe fd1[1] ditutup, kemudian nilai dari buffer bufferc dibaca dari pipe fd1[0]. Selanjutnya, data yang terdapat dalam buffer diambil dan dimasukkan ke dalam array arrc dan array freqc. Data pada array arrc merepresentasikan karakter atau simbol yang akan dikompresi dan data pada array freqc merepresentasikan frekuensi kemunculan dari karakter atau simbol tersebut.

### Penyelesaian 1B
>- Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
```c
for (int i = 0; i < ALF; ++i) {
			printf("%c: ", arrc[i]);
			for (int j = 0; codes[arrc[i]][j] != -1; ++j) {
				printf("%d", codes[arrc[i]][j]);
			}
			printf("\n");
		}

		char comp[4096]="";
		int cnt=0;
```
Potongan kode adalah bagian setelah pembentukan tabel kode Huffman. Program mencetak tabel kode Huffman yang telah terbentuk pada layar dengan menggunakan loop for. Setiap karakter pada array arrc dicetak, diikuti oleh kode Huffman yang sesuai. Dalam loop tersebut, for (int i = 0; i  ALF; ++i) digunakan untuk mengiterasi setiap karakter yang ada pada array arrc. Pada setiap iterasi, program mencetak karakter tersebut dengan printf("%c: ", arrc[i]).


### Penyelesaian 1C
>- Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
```c
int codes[256][MAX_TREE_HT];
		for (int i=0; i<256; ++i) {
			for (int j = 0; j < MAX_TREE_HT; ++j) {
				codes[i][j] = -1;
			}
		}

		HuffmanCodes(arrc, freqc, ALF, codes);
		printf("-----------------------------------------------------------------------------------------\n");

		for (int i = 0; i < ALF; ++i) {
			printf("%c: ", arrc[i]);
			for (int j = 0; codes[arrc[i]][j] != -1; ++j) {
				printf("%d", codes[arrc[i]][j]);
			}
			printf("\n");
		}
```
Potongan kode adalah proses pembentukan kode Huffman untuk setiap karakter yang terdapat pada array arrc. Pertama-tama, terdapat inisialisasi array codes dengan nilai -1. Kemudian, fungsi HuffmanCodes() dipanggil untuk membentuk kode Huffman dari setiap karakter. Fungsi HuffmanCodes() menerima argumen array arrc dan array freqc yang merepresentasikan karakter dan frekuensi kemunculannya, serta array codes yang merepresentasikan kode Huffman dari setiap karakter.

### Penyelesaian 1D
>- Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
```c
char comp[4096]="";
		int cnt=0;
		for(int i=0; i<strlen(copy); i++){
			for(int j=0; j<ALF; j++){

				if(copy[i]==arrc[j]){
					for (int l = 0; codes[arrc[j]][l] != -1; l++) {
						sprintf(comp+strlen(comp), "%d", codes[arrc[j]][l]); //concat string yang telah dienkode/ kompresi
					}
				}
			}
		}

		close(fd2[0]);
		write(fd2[1], comp, sizeof(comp));
		close(fd2[1]);
```
Potongan kode tersebut merupakan bagian dari proses kompresi data menggunakan kode Huffman. Setelah deklarasi variabel `char comp[4096]` dan `int cnt`, program melakukan loop untuk mengompresi data yang terdapat dalam string `copy` ke dalam array `comp` menggunakan tabel kode Huffman `codes`.

Pada setiap iterasi, program mencari karakter dalam `copy` di dalam `arrc`. Jika karakter tersebut ditemukan, program mengambil kode Huffman yang sesuai dari `codes` dan menggabungkannya ke dalam string `comp`. Setelah selesai mengompresi seluruh karakter dalam `copy`, data yang telah dikompresi dalam `comp` ditulis ke dalam pipe `fd2[1]`.

### Penyelesaian 1E
>- Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
```c
char Compreceive[4096];

		read(fd2[0], Compreceive, sizeof(Compreceive));
		close(fd2[0]);
		printf("\nencoded: \n%s\n", Compreceive);
		printf("\ndecoded: \n%s\n", copy);

		printf("\nTotal bit SEBELUM dilakukan kompresi huffman : %lu\n", strlen(copy)*8);
		printf("Total bit SETELAH dilakukan kompresi huffman : %lu\n", strlen(Compreceive));
```
Potongan kode ini membaca data yang telah dikompresi dari pipe `fd2[0]` ke dalam array `Compreceive`. Kemudian, pipe `fd2[0]` ditutup. Setelah itu, program mencetak data yang telah dikompresi (`Compreceive`) dan data asli (`copy`) pada layar menggunakan fungsi `printf()`.

Selanjutnya, program mencetak jumlah bit sebelum dan sesudah kompresi Huffman. `strlen(copy)` menghitung panjang string `copy`, yang merepresentasikan data asli. Karena setiap karakter membutuhkan 8 bit dalam format ASCII, maka `strlen(copy)*8` merepresentasikan total bit sebelum dilakukan kompresi. Sedangkan `strlen(Compreceive)` merepresentasikan jumlah bit setelah dilakukan kompresi. Jumlah bit setelah dilakukan kompresi seharusnya lebih sedikit daripada jumlah bit sebelumnya, karena kompresi Huffman dapat mengurangi redundansi pada data asli dan memperkecil ukuran data yang perlu disimpan atau ditransmisikan.

## Soal 2
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

### Penyelesaian 2A
>- Membuat program C dengan nama kalian.c, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

### kalian.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL1 2
#define ROW2 2
#define COL2 5
#define SHM_SIZE sizeof(int[ROW1][COL2])

int main() {
    int matriks1[ROW1][COL1], matriks2[ROW2][COL2], ans[ROW1][COL2];
    int i, j, k;

    srand(time(NULL)); // inisialisasi seed untuk random number generator

    // mengisi matriks1 dengan angka random dari 1-5 secara inklusif
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            matriks1[i][j] = rand() % 5 + 1;
        }
    }

    // mengisi matriks2 dengan angka random dari 1-4 secara inklusif
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            matriks2[i][j] = rand() % 4 + 1;
        }
    }

    // mengalikan kedua matriks
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            ans[i][j] = 0;
            for (k = 0; k < COL1; k++) {
                ans[i][j] += matriks1[i][k] * matriks2[k][j];
            }
        }
    }
```
Kode tersebut menginisialisasi matriks matriks1, matriks2, dan ans yang akan digunakan untuk menyimpan matriks input dan hasil perkalian dengan menggunakan fungsi srand(time(NULL)) untuk menginisialisasi generator angka acak dengan menggunakan waktu saat ini sebagai seed. matriks1 diisi dengan angka acak antara 1-5 (inklusif) menggunakan nested for loop. Dan matriks2 diisi dengan angka acak antara 1-4 (inklusif) menggunakan nested for loop lalu dilakukan perkalian matriks dengan menggunakan nested for loop. Setiap elemen pada matriks hasil ans dihitung dengan melakukan perkalian dot product antara baris matriks pertama matriks1 dengan kolom matriks kedua matriks2, Kemudain hasil perkalian tersebut disimpan pada elemen yang sesuai di matriks hasil ans.

```c
// mengalokasikan shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    // menghubungkan shared memory ke variabel hasil
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);
    if (shared_ans == (int (*)[COL2]) -1) {
        perror("shmat");
        return 1;
    }

    // menyalin hasil perkalian matriks ke shared memory
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            shared_ans[i][j] = ans[i][j];
        }
    }
    
    // menampilkan matriks hasil perkalian
    printf("Hasil perkalian matriks:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL2; j++) {
            printf("%d\t", ans[i][j]);
        }
        printf("\n");
    }

    shmdt(shared_ans);

    printf("Matriks hasil perkalian tersimpan dalam shared memory.\n");

    return 0;
}
```
Kode tersebut mengalokasikan shared memory menggunakan shmget dengan ukuran sebesar SHM_SIZE.Jika alokasi shared memory berhasil, langkah selanjutnya adalah menghubungkan shared memory ke variabel shared_ans menggunakan shmat.Setelah terhubung ke shared memory, matriks hasil perkalian ans disalin ke dalam shared memory dengan menggunakan nested for loop.Kemudian, matriks hasil perkalian ans ditampilkan ke layar menggunakan nested for loop dan printf.Setelah itu, koneksi ke shared memory dilepaskan menggunakan shmdt.Terakhir, ditampilkan pesan bahwa matriks hasil perkalian telah disimpan dalam shared memory. Program mengembalikan nilai 0 sebagai tanda bahwa program telah selesai dijalankan dengan sukses.

### Penyelesaian 2B-C
>- Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

### cinta.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL2 5
#define SHM_SIZE sizeof(int[ROW1][COL2])

typedef struct {
    int rows;
    int cols;
} ThreadData;

unsigned long long Factorial(int n) {
    unsigned long long ans = 1;
    for (int i = 1; i <= n; i++) {
        ans *= i;
    }
    return ans;
}

void *FactorialCount(void *arg) {
    ThreadData *data = (ThreadData *)arg;
    int rows = data->rows;
    int cols = data->cols;

    // Mengambil nilai dari shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);

    // Menghitung faktorial
    unsigned long long ans = Factorial(shared_ans[rows][cols]);

    printf("%llu\t", ans);

    shmdt(shared_ans);

    free(data);
    pthread_exit(NULL);
}
```
ThreadData digunakan untuk menyimpan informasi baris dan kolom yang akan digunakan dalam penghitungan faktorial. Fungsi Factorial menghitung faktorial dari suatu bilangan dengan menggunakan perulangan for. Fungsi FactorialCount mengambil nilai dari shared memory, menghitung faktorial dari elemen matriks yang sesuai, dan menampilkannya ke layar. Koneksi shared memory dilepaskan dan memori yang dialokasikan untuk ThreadData dibebaskan. Kode tersebut menggabungkan penggunaan thread dengan shared memory untuk melakukan perhitungan faktorial secara paralel.

```c
int main() {
    // Mengakses shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    // Menghubungkan shared memory ke variabel hasil
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);
    if (shared_ans == (int (*)[COL2]) -1) {
        perror("shmat");
        return 1;
    }

    printf("Matriks hasil perkalian:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d\t", shared_ans[i][j]);
        }
        printf("\n");
    }

    // Membuat thread untuk setiap elemen matriks
    pthread_t threads[ROW1 * COL2];
    int threadIndex = 0;

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            ThreadData *data = malloc(sizeof(ThreadData));
            data->rows = i;
            data->cols = j;

            // Membuat thread
            if (pthread_create(&threads[threadIndex], NULL, FactorialCount, (void *)data) != 0) {
                fprintf(stderr, "Error creating thread\n");
                return 1;
            }
            pthread_join(threads[i*j], NULL);
            threadIndex++;
        }
        printf("\n");
    }

    // Menunggu semua thread selesai
   // for (int i = 0; i < ROW1 * COL2; i++) {
        
    //}

    shmdt(shared_ans);

    return 0;
}
```
mengakses shared memory yang telah dialokasikan sebelumnya dan menampilkan matriks hasil perkalian ke layar. Selanjutnya, dilakukan pembuatan thread untuk setiap elemen matriks. Loop nested for digunakan untuk mengiterasi setiap elemen matriks. Dalam setiap iterasi, struktur data ThreadData dialokasikan dan diisi dengan informasi baris dan kolom elemen tersebut. Kemudian, thread dibuat menggunakan pthread_create dengan argumen berupa fungsi FactorialCount dan ThreadData. Thread-thread tersebut ditunggu hingga selesai menggunakan pthread_join untuk memastikan semua thread telah menyelesaikan perhitungan faktorial. Setelah semua thread selesai, koneksi ke shared memory dilepaskan menggunakan shmdt.

### Penyelesaian 2D
Buatlah program C ketiga dengan nama sisop.c. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada cinta.c namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

### sisop.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4
#define COL2 5
#define SHM_SIZE sizeof(int[ROW1][COL2])

unsigned long long Factorial(int n) {
    unsigned long long ans = 1;
    for (int i = 1; i <= n; i++) {
        ans *= i;
    }
    return ans;
}

int main() {
    // Mengakses shared memory
    key_t key = ftok("shared_memory_key", 'R');
    int shmid = shmget(key, SHM_SIZE, 0666);
    if (shmid == -1) {
        perror("shmget");
        return 1;
    }

    // Menghubungkan shared memory ke variabel hasil
    int (*shared_ans)[COL2] = shmat(shmid, NULL, 0);
    if (shared_ans == (int (*)[COL2]) -1) {
        perror("shmat");
        return 1;
    }

    printf("Matriks hasil perkalian:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            printf("%d\t", shared_ans[i][j]);
        }
        printf("\n");
    }

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < ROW1; i++) {
        for (int j = 0; j < COL2; j++) {
            // Menghitung faktorial dari setiap elemen matriks
            unsigned long long ans = Factorial(shared_ans[i][j]);
            
            printf("%llu\t", ans);
        }
        printf("\n");
    }

    shmdt(shared_ans);

    return 0;
}
```
Pada program ini kita tidak menggunakan thread maupun multithreading sehingga didapat hasil runtime yang lebih cepat dibanding menggunakan thread pada sisop.c

### Output
![Screenshot 2023-05-13 180636](https://github.com/kenanargya/P2_Komnum_B3/assets/92387421/de5dae35-8da5-404c-9be7-0c382f6ad352)

## Soal 3
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun, Elshe tidak paham harus mulai dari mana.

### Penyelesaian 3A
>- Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan user (multiple sender dengan identifier) user.c menggunakan message queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan oleh sistem.

### user.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/sem.h>

#define MSG_SIZE 2048

struct message
{
  long type;
  char text[1024];
};

union semun
{
  int val;
  struct semid_ds *buf;
  unsigned short *array;
};

int main()
{
  int msgid;
  key_t key = ftok("stream.c", 'A');
  if (key == -1)
  {
    perror("ftok");
    exit(1);
  }
  msgid = msgget(key, 0666 | IPC_CREAT);
  if (msgid == -1)
  {
    perror("msgget");
    exit(1);
  }
  . . .
  . . .
```
Secara singkatnya, `user.c` berisi program yang akan mengirim command ke `stream.c` kemudian command tersebut akan diterima oleh `stream.c` dan dijalankan. Pertama-tama, dilakukan inisialisasi message queue terlebih dahulu. Digunakan fungsi `ftok()` untuk menghasilkan key dari `stream.c` dan karakter `A`. Kemudian, inisialisasi msgid (sebagai id message queue) yang memanggil fungsi `msgget` dengan parameter key dan hak akses `0666 | IPC_CREAT` .

```c
  . . .
  . . .
  int semid;
  union semun arg;
  struct sembuf sb = {0, -1, 0}; // Wait operation
  key_t semkey = ftok("user.c", 'B');
  if (semkey == -1)
  {
    perror("ftok");
    exit(1);
  }
  semid = semget(semkey, 1, IPC_CREAT | 0666);
  if (semid == -1)
  {
    perror("semget");
    exit(1);
  }
  arg.val = 2; // Set semaphore value to 2 (number of allowed users)
  if (semctl(semid, 0, SETVAL, arg) == -1)
  {
    perror("semctl");
    exit(1);
  }
  . . .
  . . .
```
Selanjutnya, dilakukan inisialisasi semaphore dengan semkey yang akan menghasilkan key. Sama seperti message queue tadi, semid (id semaphore) berisi fungsi smget dengan parameter semkey dan akses `IPC_CREAT | 0666`. `arg.val = 2` mengatur nilai semaphore menjadi 2 (sesuai permintaan soal dimana hanya 2 user yang diizinkan untuk mengirim command) menggunakan `semctl()`.


```c
  char command[MSG_SIZE];
 
  pid_t pid = getpid();
  printf("User %d connected to stream.\n", pid);

  while (1)
  {
    // Wait for semaphore to become available
    struct sembuf sem_wait = {0, -1, 0};
    semop(semid, &sem_wait, 1);
```
inisialisasi variable `command` dan `pid`. Di dalam while loop, program menunggu dan meminta resource yang tersedia menggunakan operasi wait pada semaphore dengan `semop()`.


```c
    . . .
    printf("Enter command: ");
    fgets(command, MSG_SIZE, stdin);
    strtok(command, "\n");

    struct message msg;
    msg.type = pid;

    if (strncmp(command, "DECRYPT", 7) == 0)
    {
      strcpy(msg.text, "DECRYPT");
    }
    else if (strncmp(command, "LIST", 4) == 0)
    {
      strcpy(msg.text, "LIST");
    }
    else if (strncmp(command, "PLAY", 4) == 0)
    {
      char *song = strtok(command + 5, "\"");
      if (song == NULL)
      {
        printf("Invalid command: %s\n", command);
        continue;
      }
      // Convert song to uppercase for case-insensitive comparison
      for (int i = 0; i < strlen(song); i++)
      {
        song[i] = toupper(song[i]);
      }
      sprintf(msg.text, "PLAY %s", song);
    }
    else if (strncmp(command, "ADD", 3) == 0)
    {
      char *song = strtok(command + 4, "\n");
      if (song == NULL)
      {
        printf("Invalid command: %s\n", command);
        continue;
      }
      sprintf(msg.text, "ADD %s", song);
    }
    else
    {
      printf("Unknown command: %s\n", command);
      continue;
    }

    if (msgsnd(msgid, &msg, sizeof(msg), 0) == -1)
    {
      perror("msgsnd");
      exit(1);
    }
```
Digunakan `fgets` untuk meminta input dari user dalam bentuk string. String yang diterima akan diproses dengan `strtok()` untuk memisahkan karakter "\n" dari akhir string. String hasil input dimasukkan ke dalam command kemudian membandingkan string command menggunakan `strncmp`. Jika kondisi terpenuhi saat kedua string dibandingkan dan bernilai sama (string yang diinput sesuai dengan ketentuan soal) maka akan dilakukan `strcpy()` untuk command `DECRPT` dan `LIST` serta `sprintf()` untuk command `PLAY` dan `ADD`. Pada command `PLAY` dilakukan for loop untuk mengubah lagu menjadi uppercase menggunakan fungsi `toupper()` untuk perbandingan case insensitive. Setelah input command, `msgsnd()` akan mengirim pesan ke message queue.


```c
    // Release semaphore
    struct sembuf sem_signal = {0, 1, 0};
    semop(semid, &sem_signal, 1);
  }

  // Remove message queue and semaphore
  msgctl(msgid, IPC_RMID, NULL);
  semctl(semid, 0, IPC_RMID, arg);

  return 0;
}
```
Terakhir pada user.c ini, semaphore tadi akan direlease menggunakan `semop()` agar dapat diakses proses lain. Kemudian, message queue dan semaphore akan dihapus menggunakan fungsi `msgctl()` dan `semctl()`.

# stream.c

```c
int main()
{
  const char *filename = "song-playlist.json";

  int msgid;
  key_t key = ftok("stream.c", 'A');
  msgid = msgget(key, 0666 | IPC_CREAT);
  if (msgid == -1)
  {
    perror("Error creating message queue");
    exit(1);
  }
. . .
. . .
. . .
while (1)
  {
    // Menerima user command dari mqueue
    struct message message;
    if (msgrcv(msgid, &message, sizeof(message), 0, 0) == -1)
    {
      perror("Error receiving message");
      exit(1);
    }
. . .
. . .
```

Pada `stream.c`, kurang lebih sama seperti `user.c` yaitu pertama dilakukan inisialisasi message queue terlebih dahulu dengan `ftok()` yang sama. Di dalam while loop, `msgrcv()` dipakai untuk menerima command dari message queue yang dikirim oleh `user.c`.


### Penyelesaian 3B

>- User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem stream akan melakukan decrypt/decode/konversi pada file song-playlist.json (dapat diunduh manual saja melalui link berikut) sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet. Proses decrypt dilakukan oleh program stream.c tanpa menggunakan koneksi socket.

Soal meminta untuk melakukan decrypt menggunakan 3 metode yaitu rot13, base64, dan hex. Berikut adalah fungsi-fungsi yang digunakan untuk melakukan decrypt:

```c
static char *rot13_decode(const char *input)
{
    int len = strlen(input);
    char *output = malloc(len + 1);
    if (output == NULL)
    {
        return NULL;
    }
    const char *input_ptr = input;
    char *output_ptr = output;
    while (*input_ptr)
    {
        char c = *input_ptr++;
        if (isalpha(c))
        {
            if (islower(c))
            {
                c = 'a' + ((c - 'a' + 13) % 26);
            }
            else
            {
                c = 'A' + ((c - 'A' + 13) % 26);
            }
        }
        *output_ptr++ = c;
    }
    *output_ptr = '\0';
    return output;
}
```
Fungsi di atas berisi kode untuk melakukan dekripsi dengan metode rot13. Pertama, fungsi ini akan menghitung panjang dari input string dengan menggunakan fungsi `strlen` dan kemudian mengalokasikan memori yang cukup untuk output string dengan menggunakan fungsi `malloc`. Selanjutnya, fungsi akan melakukan decode terhadap setiap karakter dalam input string satu per satu menggunakan loop `while` dan pointer `input_ptr` untuk mengakses karakter pada input string. Jika karakter tersebut adalah huruf (diperiksa menggunakan fungsi isalpha), maka karakter tersebut akan diubah sesuai aturan ROT13 menggunakan rumus (c - 'a' + 13) % 26 untuk huruf kecil dan (c - 'A' + 13) % 26 untuk huruf besar untuk menggeser huruf sebesar 13 alfabet. Karakter hasil decode akan disimpan pada output string yang ditunjuk oleh pointer `output_ptr`.

```c
static const unsigned char base64_table[256] = {
    ['A'] = 0, ['B'] = 1, ['C'] = 2, ['D'] = 3, ['E'] = 4, ['F'] = 5, ['G'] = 6, ['H'] = 7, ['I'] = 8, ['J'] = 9, ['K'] = 10, ['L'] = 11, ['M'] = 12, ['N'] = 13, ['O'] = 14, ['P'] = 15, ['Q'] = 16, ['R'] = 17, ['S'] = 18, ['T'] = 19, ['U'] = 20, ['V'] = 21, ['W'] = 22, ['X'] = 23, ['Y'] = 24, ['Z'] = 25, ['a'] = 26, ['b'] = 27, ['c'] = 28, ['d'] = 29, ['e'] = 30, ['f'] = 31, ['g'] = 32, ['h'] = 33, ['i'] = 34, ['j'] = 35, ['k'] = 36, ['l'] = 37, ['m'] = 38, ['n'] = 39, ['o'] = 40, ['p'] = 41, ['q'] = 42, ['r'] = 43, ['s'] = 44, ['t'] = 45, ['u'] = 46, ['v'] = 47, ['w'] = 48, ['x'] = 49, ['y'] = 50, ['z'] = 51, ['0'] = 52, ['1'] = 53, ['2'] = 54, ['3'] = 55, ['4'] = 56, ['5'] = 57, ['6'] = 58, ['7'] = 59, ['8'] = 60, ['9'] = 61, ['+'] = 62, ['/'] = 63};

// Base64 decoding function
static char *base64_decode(const char *input)
{
    int len = strlen(input);
    if (len % 4 != 0)
    {
        return NULL;
    }

    int output_len = 3 * (len / 4);
    if (input[len - 1] == '=')
    {
        output_len--;
    }
    if (input[len - 2] == '=')
    {
        output_len--;
    }

    char *output = (char *)malloc(output_len + 1);
    if (output == NULL)
    {
        return NULL;
    }

    for (int i = 0, j = 0; i < len;)
    {
        uint32_t sextet_a = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];
        uint32_t sextet_b = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];
        uint32_t sextet_c = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];
        uint32_t sextet_d = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
                        + (sextet_b << 2 * 6)
                        + (sextet_c << 1 * 6)
                        + (sextet_d << 0 * 6);

        if (j < output_len)
        {
            output[j++] = (triple >> 2 * 8) & 0xFF;
        }
        if (j < output_len)
        {
            output[j++] = (triple >> 1 * 8) & 0xFF;
        }
        if (j < output_len)
        {
            output[j++] = (triple >> 0 * 8) & 0xFF;
        }
    }

    output[output_len] = '\0';
    return output;
}
. . .
. . .
```
Fungsi di atas merupakan fungsi untuk melakukan decrypt dengan metode Base64. Fungsi ini menggunakan tabel konversi `base64_table` yang berisi indeks dan nilai yang merepresentasikan karakter-karakter Base64. Pertama-tama, fungsi melakukan pemeriksaan apakah panjang input string habis dibagi 4 dan mengembalikan NULL jika tidak. Selanjutnya, fungsi menghitung panjang output yang dihasilkan dari decoding dengan rumus output_len = 3 * (len / 4), dimana len adalah panjang dari input string. Jika ada satu atau dua karakter padding ('=') pada akhir input string, maka output_len akan dikurangi sesuai dengan jumlah karakter padding tersebut. Setelah itu, fungsi membuat buffer output sebesar output_len ditambah satu untuk karakter null-terminator dan mengalokasikan memori untuk buffer tersebut.

Selanjutnya, fungsi melakukan iterasi sepanjang input string dalam kelipatan empat karakter. Untuk setiap kelompok empat karakter, fungsi mengkonversi masing-masing karakter menjadi sebuah nilai bilangan yang merepresentasikan karakter tersebut dalam bentuk Base64, menggunakan tabel konversi `base64_table`. Kemudian, fungsi menggabungkan keempat nilai tersebut menjadi sebuah bilangan tiga-byte dan mengekstrak tiga byte tersebut dari bilangan tersebut dan menuliskannya ke output buffer. Setelah iterasi selesai, fungsi menambahkan karakter null-terminator ke output buffer dan mengembalikan output buffer tersebut.

```c
static unsigned char* hex_decode(const char* input) {
  int len = strlen(input);
  if (len % 2 != 0) {
    return NULL;
  }

  int output_len = len / 2;
  char* output = malloc(output_len + 1);
  if (output == NULL) {
    return NULL;
  }

  for (int i = 0; i < len; i += 2) {
    char c1 = input[i];
    char c2 = input[i + 1];

    int value = 0;
    if (c1 >= '0' && c1 <= '9') {
      value += (c1 - '0') * 16;
    } else if (c1 >= 'a' && c1 <= 'f') {
      value += (c1 - 'a' + 10) * 16;
    } else if (c1 >= 'A' && c1 <= 'F') {
      value += (c1 - 'A' + 10) * 16;
    } else {
      free(output);
      return NULL;
    }

    if (c2 >= '0' && c2 <= '9') {
      value += c2 - '0';
    } else if (c2 >= 'a' && c2 <= 'f') {
      value += c2 - 'a' + 10;
    } else if (c2 >= 'A' && c2 <= 'F') {
      value += c2 - 'A' + 10;
    } else {
      free(output);
      return NULL;
    }

    output[i / 2] = (char)value;
  }

  output[output_len] = '\0';
  return output;
}
. . .
```
Fungsi di atas adalah fungsi yang akan digunakan untuk melakukan decrypt dengan metode hex. Fungsi ini bertujuan untuk mengkonversi string hexadecimal menjadi string ASCII. Pertama-tama, fungsi akan memeriksa apakah panjang dari string input adalah bilangan genap. Jika tidak, maka akan mengembalikan nilai NULL karena sebuah string hexadecimal harus memiliki bilangan karakter yang genap. Selanjutnya, fungsi akan menghitung panjang string output yang dihasilkan dengan membagi panjang string input dengan 2. Kemudian, fungsi akan mengalokasikan memory untuk string output menggunakan fungsi `malloc()`.

Fungsi akan melakukan iterasi sebanyak panjang string input dengan penambahan 2 pada setiap iterasi, karena setiap karakter hexadecimal akan terdiri dari 2 karakter ASCII. Fungsi akan mengambil 2 karakter pertama dari setiap pasangan karakter hexadecimal dan mengkonversinya ke dalam bilangan decimal menggunakan nilai ASCII dari setiap karakter. Jika karakter pertama bukan merupakan angka hexadecimal (0-9 atau a-f atau A-F), maka fungsi akan membebaskan memory yang telah dialokasikan dan mengembalikan nilai NULL. Hal yang sama dilakukan jika karakter kedua juga tidak merupakan angka hexadecimal.

Jika kedua karakter tersebut adalah angka hexadecimal, maka fungsi akan mengkonversi keduanya ke dalam bilangan decimal dan kemudian menggabungkannya menjadi sebuah bilangan decimal tunggal. Fungsi akan menyimpan nilai tersebut pada array output pada `indeks i/2`. Setelah selesai melakukan iterasi, fungsi akan menambahkan karakter NULL pada akhir array output dan mengembalikan pointer ke array tersebut.

Berikut adalah proses pemanggilan fungsi-fungsi di atas pada `int main()` untuk menjalankan command `DECRYPT`:

```c
// int main () {
. . .
. . .
if (strcmp(command, "DECRYPT") == 0)
    {
      FILE *fp = fopen(filename, "r");
      if (fp == NULL)
      {
        printf("Error opening file: %s\n", filename);
        return 1;
      }
      fseek(fp, 0, SEEK_END);
      long file_size = ftell(fp);
      fseek(fp, 0, SEEK_SET);
      char *json_str = malloc(file_size + 1);
      if (json_str == NULL)
      {
        printf("Error allocating memory\n");
        return 1;
      }
      fread(json_str, 1, file_size, fp);
      fclose(fp);
      json_str[file_size] = '\0';

      cJSON *root = cJSON_Parse(json_str);
      if (root == NULL)
      {
        printf("Error parsing JSON: %s\n", cJSON_GetErrorPtr());
        free(json_str);
        return 1;
      }

      FILE *playlist = fopen("playlist.txt", "w");
      if (playlist == NULL)
      {
        printf("Error opening playlist file for writing\n");
        cJSON_Delete(root);
        free(json_str);
        return 1;
      }

      cJSON *item = NULL;
      cJSON_ArrayForEach(item, root)
      {
        cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
        cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");

        if (strcmp(method->valuestring, "base64") == 0)
        {
          char *decoded = base64_decode(song->valuestring);
          if (decoded == NULL)
          {
            printf("Error decoding base64: %s\n", song->valuestring);
            continue;
          }
          
          fprintf(playlist, "%s\n", decoded);
          free(decoded);
        }
        else if (strcmp(method->valuestring, "hex") == 0)
        {
          char *decoded = hex_decode(song->valuestring);
          if (decoded == NULL)
          {
            printf("Error decoding hex: %s\n", song->valuestring);
            continue;
          }
          
          fprintf(playlist, "%s\n", decoded);
          free(decoded);
        }
        else if (strcmp(method->valuestring, "rot13") == 0)
        {
          char *decoded = rot13_decode(song->valuestring);
          if (decoded == NULL)
          {
            printf("Error decoding rot13: %s\n", song->valuestring);
            continue;
          }
          
          fprintf(playlist, "%s\n", decoded);
          free(decoded);
        }
        else
        {
          printf("Unknown method: %s\n", method->valuestring);
          continue;
        }
    }


    fclose(playlist);
    cJSON_Delete(root);
    free(json_str);

    system("sort playlist.txt > playlist_sorted.txt");
    system("cp playlist_sorted.txt playlist.txt");
    system("rm playlist_sorted.txt");

    }
    . . .

```
Jika kondisi `if` terpenuhi yaitu perbandingan string command dan "DECRYPT" bernilai sama, maka akan masuk ke kode untuk menjalankan command. Pertama, file `song-playlist.json` akan dibuka untuk dibaca lalu dengan fungsi `fseek()` dan `fread()` isi file JSON akan dibaca. Kemudian, dilakukan parsing JSON string `json_str` ke dalam bentuk object JSON menggunakan `cJSON_Parse()`. 

Setelah itu, menggunakan fungsi cJSON yaitu `cJSON_GetObjectItemCaseSensitive()` untuk mendapatkan item dari file JSON yang mengandung `method` dan `song`. Selanjutnya, dalam if condition dilakukan perbandingan string antara valuestring dari method dengan string masing-masing method `("rot13", "base64", "hex")` menggunakan `strcmp()`. Jika bernilai sama maka akan dipanggil fungsi sesuai methodnya, hasil dari dekripsi akan disimpan dalam `playlist.txt` menggunakan `fprintf()`. Setiap selesai dilakukan dekripsi, digunakan `free()` untuk mengosongkan memori agar terhindar dari memory leak. 

Setelah decrypt berjalan semua, file `playlist.txt` akan ditutup dan digunakan `system` untuk melakukan `sort playlist.txt` yang sementara disimpan ke `playlist_sorted.txt` kemudian mengembalikan isi `playlist_sorted.txt` ke `playlist.txt` dengan command `cp` lalu terakhir hapus `playlist_sorted.txt` tadi dengan command `rm`.


### Penyelesaian 3C

>- Selain itu, user dapat mengirimkan perintah LIST, kemudian sistem stream akan menampilkan daftar lagu yang telah di-decrypt

```c
else if (strcmp(command, "LIST") == 0)
    {
      system("cat playlist.txt");
    }
. . .
```
Untuk command `LIST`, penyelesaian yang dilakukan cukup sederhana dengan menggunakan `system` dan command `cat` untuk menampilkan `playlist.txt` karena sebelumnya lagu-lagu di file tersebut telah disort oelh command DECRYPT.

### Penyelesaian 3D

> User juga dapat mengirimkan perintah PLAY <SONG> dengan ketentuan sebagai berikut.

- PLAY "Stereo Heart" 
    sistem akan menampilkan: 
    USER <USER_ID> PLAYING "GYM CLASS HEROES - STEREO HEART"

- PLAY "BREAK"
    sistem akan menampilkan:
    THERE ARE "N" SONG CONTAINING "BREAK":
    1. THE SCRIPT - BREAKEVEN
    2. ARIANA GRANDE - BREAK FREE
dengan “N” merupakan banyaknya lagu yang sesuai dengan string query. Untuk contoh di atas berarti THERE ARE "2" SONG CONTAINING "BREAK":

- PLAY "UVUWEVWEVWVE"
    THERE IS NO SONG CONTAINING "UVUVWEVWEVWE"

```c
. . .
else if (strcmp(command, "PLAY") == 0)
    {
      // Search for songs containing the query
      char *query = param;
      int num_matches = 0;
      char matches[MAX_SONGS][MAX_SONG_NAME_LENGTH];
      FILE *playlist = fopen("playlist.txt", "r");
      if (playlist == NULL)
      {
        printf("Error opening playlist file for reading\n");
        return 1;
      }
      char song_name[MAX_SONG_NAME_LENGTH];
      while (fgets(song_name, MAX_SONG_NAME_LENGTH, playlist) != NULL)
      {
        // Remove trailing newline if present
        song_name[strcspn(song_name, "\n")] = '\0';

        // Check if song name contains the query
        char *found = strcasestr(song_name, query);
        if (found != NULL)
        {
          strcpy(matches[num_matches], song_name);
          num_matches++;
        }
      }
      fclose(playlist);

      // Print search results
      if (num_matches == 0)
      {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", query);
      }
      else if (num_matches > 1)
      {
        printf("THERE ARE \"%d\" SONG(S) CONTAINING \"%s\":\n", num_matches, query);
        for (int i = 0; i < num_matches; i++)
        {
          printf("%d. %s\n", i + 1, matches[i]);
        }
      }
      else if (num_matches == 1)
      {
        // Play the first matching song
        printf("USER <%ld> PLAYING \"%s\"\n", message.mtype, matches[0]);
      }
    }  
. . .
```
Pada bagian ini, program melakukan pencarian lagu pada file `playlist.txt` dengan menggunakan sebuah query yang diberikan oleh pengguna melalui parameter command. Program membuka file `playlist.txt` dan membaca setiap baris dalam file tersebut. Kemudian, program memeriksa apakah nama lagu tersebut mengandung query yang diberikan oleh pengguna dengan menggunakan fungsi `strcasestr()`. Jika nama lagu mengandung query, maka program akan menyimpan nama lagu tersebut dalam array `matches`.

Setelah selesai membaca setiap baris dalam file `playlist.txt`, program akan menutup file tersebut dan memeriksa berapa banyak lagu yang ditemukan yang mengandung query tersebut. Jika tidak ada lagu yang ditemukan, program akan mencetak pesan `"THERE IS NO SONG CONTAINING"` diikuti dengan query (judul lagu) yang diberikan oleh pengguna. Jika terdapat lebih dari satu lagu yang ditemukan, program akan mencetak pesan `"THERE ARE (jumlah lagu) SONG(S) CONTAINING (judul lagu/kata yang dicari user)"` serta mencetak setiap nama lagu yang ditemukan. Jika hanya terdapat satu lagu yang ditemukan, program akan mencetak pesan `"USER <ID PENGIRIM PESAN> PLAYING (judul lagu)"`.

### Penyelesaian 3E
>- User juga dapat menambahkan lagu ke dalam playlist dengan syarat sebagai berikut:

- User mengirimkan perintah
ADD <SONG1>
ADD <SONG2>
sistem akan menampilkan:
USER <ID_USER> ADD <SONG1>

>- User dapat mengedit playlist secara bersamaan tetapi lagu yang ditambahkan tidak boleh sama. Apabila terdapat lagu yang sama maka sistem akan meng-output-kan “SONG ALREADY ON PLAYLIST”

Untuk penyelesaian ini, dibuat fungsi `add_playlist()` untuk mengeksekusi penambahan lagu ke palylist.

```c
. . .
. . .

void add_playlist(char *song_name)
{
  // Open the playlist file for reading
  FILE *playlist = fopen("playlist.txt", "r");
  if (playlist == NULL)
  {
    printf("Error opening playlist file for reading\n");
    return 0;
  }

  // Check if song is already in the playlist
  char line[MAX_SONG_NAME_LENGTH];
  while (fgets(line, MAX_SONG_NAME_LENGTH, playlist) != NULL)
  {
    line[strcspn(line, "\n")] = '\0'; // Remove newline character
    if (strcasecmp(line, song_name) == 0)
    {
      printf("SONG ALREADY ON PLAYLIST\n");
      fclose(playlist);
      return 0;
    }
  }
 
  fclose(playlist);

  // Open the playlist file for appending
  playlist = fopen("playlist.txt", "a");
  if (playlist == NULL)
  {
    printf("Error opening playlist file for appending\n");
    return 0;
  }

  fprintf(playlist, "%s\n", song_name);

  fclose(playlist);

  return 1;
}
. . .
```
Fungsi ini diawali dengan membuka file `playlist.txt` untuk dibaca. Selanjutnya, dilakukan pemeriksaan apakah lagu sudah ada di playlist atau belum dengan menggunakan fungsi `strcasecmp()`. Jika ditemukan string yang sama maka akan menampilkan output `SONG ALREADY ON PLAYLIST` dan menutup filenya. Untuk menambahkan lagu ke playlist, dibuka `playlist.txt` dengan command `"a"` untuk append atau menambahkan nama lagu yang diberikan user ke dalam file yang sudah ada dan mencetaknya menggunakan `fprintf()`. Untuk memanggil fungsi ini, pada `main()` dilakukan:

```c
else if (strcmp(command, "ADD") == 0)
    {
      add_playlist(param);
      if(1) {
        printf("USER <%ld> ADD %s\n", message.mtype, param);
      }
        
    }
```
Jika kondisi if terpenuhi maka fungsi `add_playlist()` akan dipanggil dengan parameter param kemudian jika condition bernilai true maka akan mencetak `USER <ID> ADD <JUDUL LAGU>`.

### Penyelesaian 3F
>- Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga mengirim perintah apapun.

```c
    key_t sem_key = ftok("user.c", 'B');
    int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
    if (sem_id == -1)
    {
        perror("semget failed");
        exit(1);
    }

  // Initialize semaphore (user) to 2 

    union semun sem_union;
    sem_union.val = 2;
    if (semctl(sem_id, 0, SETVAL, sem_union) == -1) {
        perror("semctl failed");
        exit(EXIT_FAILURE);
    }
    int user_count;
    struct sembuf sembuf_array[1];
```
Kode di atas adalah kode untuk inisialisasi semaphore yang akan digunakan. `sem_union.val = 2` akan menginisialisasi semaphore yang akan membatasi user atau resource sebesar 2. 

```c
// while(1) {
. . .
. . .
    sembuf_array[0].sem_num = 0;
    sembuf_array[0].sem_op = -1;
    sembuf_array[0].sem_flg = SEM_UNDO;
    if (semop(sem_id, sembuf_array, 1) == -1) {
      if (errno == EINTR) {
        continue;
      }
      perror("semop failed");
      exit(EXIT_FAILURE);
    }

    sem_union.val = 0;
    user_count = semctl(sem_id, 0, GETVAL, sem_union);
    if (user_count < 0) {
        perror("semctl failed");
        exit(EXIT_FAILURE);
    }
    if (user_count < 2) {
        sembuf_array[0].sem_op = 1;
        if (semop(sem_id, sembuf_array, 1) == -1) {
            if (errno == EINTR) {
            continue;
            }
            perror("semop failed");
            exit(EXIT_FAILURE);
        }
    } else {
        printf("STREAM SYSTEM OVERLOAD\n");
        continue;
    }
. . .
. . .
```
`semop()` akan mengakses operasi semaphore dengan mengirimkan array sembuf (dalam kasus ini, sembuf_array) ke id semaphore yang ditentukan (sem_id). `sem_union.val = 0` mengatur nilai val pada union semun (semctl) yang akan digunakan untuk meminta nilai semaphore yang terkait. `user_count = semctl(sem_id, 0, GETVAL, sem_union)` meminta nilai semaphore dengan nomor indeks 0 (dalam kasus ini) dari semaphore array yang ditentukan (dalam kasus ini, sem_id). Jika nilai semaphore tidak berhasil diambil, program akan menampilkan pesan kesalahan dan keluar dari program.

Jika `user_count < 2`, maka akan diatur operasi semaphore pada indeks 0 agar nilai semaphore bertambah 1 yang akan memenuhi sumber daya. Jika resource/sumber daya sudah penuh maka akan menampilkan output `STREAM SYSTEM OVERLOAD`.

```C
. . .
. . .
    sembuf_array[0].sem_num = 0;
    sembuf_array[0].sem_op = 1;
    sembuf_array[0].sem_flg = SEM_UNDO;
    if (semop(sem_id, sembuf_array, 1) == -1) {
      if (errno == EINTR) {
        continue;
      }
      perror("semop failed");
      exit(EXIT_FAILURE);
    }
}
```
Pada akhir while loop, dilakukan release semaphore agar proses lain bisa berjalan setelah menunggu proses sebelumnya selesai.

### Penyelesaian 3G
>- Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan menampilkan "UNKNOWN COMMAND".

Penyelesaian soal ini terdapat pada `user.c`
```c
. . .
else
    {
      printf("Unknown command: %s\n", command);
      continue;
    }
. . .
```
Pada bagian `if-else` condition untuk menerima input command dari user, di paling bawah terdapat `else` yang akan menampilkan output `"UNKNOWN COMMAND"` jika input yang diberikan user tidak sesuai dengan string yang sudah dibandingkan di atasnya yaitu `DECRYPT, LIST, PLAY, ADD`.

### Revisi

Ketika praktikum kemarin, kode hanya berhasil sampai nomor 3C dan ouput decrypt masih kurang sesuai dengan yang soal minta. Maka dari itu, dilakukan revisi berupa merombak ulang semua kode dari awal sehingga sekarang hasilnya bisa menjalankan semua command yang diminta soal.

### Kendala

Ketika mengerjakan praktikum dan revisi, kendala yang paling berat ada pada:

- Membaca dan mendapatkan string dari JSON file
- Melakukan decrypt sesuai method yang diharapkan
- Semaphore untuk membatasi user yang bisa mengakses playlist

## Soal 4
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama hehe.zip. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama extensions.txt dan max.txt. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 
>> - Download dan unzip file tersebut dalam kode c bernama unzip.c.
>> - Selanjutnya, buatlah program categorize.c untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file extensions.txt. Buatlah folder categorized dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file max.txt, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format extension (2), extension (3), dan seterusnya.
>>- Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
extension_a : banyak_file
extension_b : banyak_file
extension_c : banyak_file
other : banyak_file
>>- Setiap pengaksesan folder, sub-folder, dan semua folder pada program categorize.c wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.
>> - Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.
DD-MM-YYYY HH:MM:SS ACCESSED [folder path]
DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]
DD-MM-YYYY HH:MM:SS MADE [folder name]
examples : 
02-05-2023 10:01:02 ACCESSED files
02-05-2023 10:01:03 ACCESSED files/abcd
02-05-2023 10:01:04 MADE categorized
02-05-2023 10:01:05 MADE categorized/jpg
02-05-2023 10:01:06 MOVED jpg file : files/abcd/foto.jpg > categorized/jpg
Catatan:
Path dimulai dari folder files atau categorized
Simpan di dalam log.txt
ACCESSED merupakan folder files beserta dalamnya
Urutan log tidak harus sama
>>- Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama logchecker.c untuk mengekstrak informasi dari log.txt dengan ketentuan sebagai berikut.
Untuk menghitung banyaknya ACCESSED yang dilakukan.
Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

### Pembahasan File unzip.c
```c
void download_file(char downloadLink[], char outputName[]) {
    if (fork() == 0) {
        char *arguments[] = {"wget", downloadLink, "-O", outputName, NULL};
        execv("/bin/wget", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0);
    return;
}
```
Kode ini merupakan sebuah fungsi yang bernama ```download_file``` yang menerima dua parameter string yaitu ```downloadLink``` sebagai link download dan ```outputName``` sebagai nama untuk folder yang didownload. Fungsi ini bertujuan untuk mendownload file dari suatu link dengan menggunakan perintah wget.

- Pertama yang dilakukan adalah mendeklarasi satu integer yaitu ```status``` sebagai status keluaran dari proses yang dijalankan.
- Kedua, terdapat fungsi ```fork()``` untuk membuat _child process_. Jika nilai ```fork()```adalah 0, maka _child process_ akan dijalankan. Sehingga argumen perintah ```wget``` diatur di dalam array ```argv```.
- Ketiga terdapat fungsi ```execv()``` yang berguna untuk menjalankan perintah ```wget``` dengan argumen yang ditentukan dan menjalankan program yang berada di path tertentu. ```NULL``` menandakan akhir dari array argv.
- Jika fungsi ```fork()``` tidak sama dengan 0, maka yang dijalankan adalah _parent process_, sehingga fungsi ```wait()``` akan dipanggil untuk menunggu _child process_ selesai. Ketika selesai status akan disimpan, lalu kontrol akan kembali ke _parent process_ sambil menunggu _child process_ selesai.

```c
void unzip(char zipName[]) {
    if (fork() == 0) {
        char *arguments[] = {"unzip", zipName, NULL};
        execv("/usr/bin/unzip", arguments);
        exit(EXIT_SUCCESS);
    }
    int status;
    while (wait(&status) >= 0);
    return;
}
```
Kode ini merupakan sebuah fungsi yang bernama ```unzip``` digunakan untuk unzip sebuah file zip ke dalam folder. Fungsi ini menggunakan dua parameter string yaitu ```zipName``` sebagai nama file zip yang akan di-unzip.

Cara kerja kode ini juga mirip dengan fungsi ```download_file``` sebelumnya.
- Pertama yang dilakukan adalah mendeklarasi satu integer yaitu ```status``` sebagai status keluaran dari proses yang dijalankan.
- Kedua digunakan fungsi ```fork()``` untuk membuat _child process_ baru yang akan menjalankan perintah ```unzip``` menggunakan ```execv()```. 
- Dalam fungsi ini terdapat perintah ```-d``` untuk menentukan direktori mana sebagai tempat file setelah di-unzip.
- Untuk perintah ```wait()``` juga sama dengan fungsi yang terdapat di ```download_file```. _Parent process_ akan menunggu _child process_ selesai, lalu fungsi akan mengembalikan nilai.

```c
int main () {
	download_file("https://docs.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp","hehe.zip");
	unzip("hehe.zip");
}
```
Pada fungsi main dipanggil fungsi ```download_file``` yang berisi dua parameter yaitu link download dan nama file zip setelah di-_download_ yaitu ```hehe.zip```. Setelah itu dipanggil fungsi unzip yang memiliki satu parameter yaitu ```hehe.zip``` untuk melakukan unzip pada file tersebut.

###Pembahasan File categorize.c 
```c
struct ThreadArgs {
    char dirname[MAX_LEN];//menyimpan nama direktori
    int maxFile;
};
```
- Dalam kode tersebut, terdapat definisi dari sebuah struct bernama ```ThreadArgs``` yang terdiri dari dua variabel, yaitu ```dirname``` dan ```maxFile```. Variabel ```dirname``` merupakan array karakter dengan kapasitas ```MAX_LEN``` yang digunakan untuk menyimpan nama direktori yang akan dibuat.

- Variabel ```maxFile``` merupakan sebuah nilai numerik bulat yang menentukan batasan jumlah file yang dapat ada di dalam satu direktori. Dalam kode tersebut, struct ```ThreadArgs``` akan digunakan sebagai argumen untuk setiap thread yang dibuat pada tahap pembuatan direktori untuk setiap ekstensi.

```c
void writeLogMade(char createdDir[]){
    // menulis pesan log
    time_t now = time(NULL); //mendapatkan informasi waktu dan nyimpan di variabel now
    struct tm *t = localtime(&now); //waktu diubah menjadi string format tanggal dan waktu dengan menggunakan fungsi localtime dan strftime
    char logMADE[256] = "MADE "; //variabel akan digabungkan dengan nama direktori yang dibuat untuk membentuk pesan log
    char _time_str[80]; 
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logMADE, createdDir);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logMADE); //nulis pesan log ke dalam file log.txt
    pthread_mutex_unlock(&lock); //membuka penggunaan mutex yang sebelumnya dikunci sehingga mutex bisa digunakan oleh thread lain
}
```
Dalam program ini, terdapat fungsi ```writeLogMade``` yang berfungsi untuk mencatat pesan log ke file ```log.txt``` ketika pembuatan direktori berhasil dilakukan. Isi dari pesan log tersebut mencakup informasi mengenai waktu pembuatan direktori dan nama direktori yang berhasil dibuat oleh program.

```c
void writeLogAccess(char folderPath[]){
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char logAccess[256] = "ACCESSED ";
    char _time_str[80];
    strftime(_time_str, sizeof(_time_str), "%d-%m-%Y %H:%M:%S", t);

    strcat(logAccess, folderPath);
    pthread_mutex_lock(&lock);
    fprintf(log_file, "%s %s\n", _time_str, logAccess);
    pthread_mutex_unlock(&lock);
}
```
Definisi kode di atas merupakan fungsi ```writeLogAccess``` yang berperan dalam menuliskan pesan log ke dalam file log. Fungsi ini menerima satu argumen yaitu ```folderPath```, yang nantinya akan digunakan untuk mencatat pesan log mengenai akses yang dilakukan terhadap folder tertentu.

```c
void* create_directory(void* arg) { 
    struct ThreadArgs* thread_args = (struct ThreadArgs*) arg; //argumen arg dikonversi menjadi ponter ke struktur Thread args
    char* dirname = thread_args->dirname; //nilai nama direktori disimpan di dirname
    int maxFile = thread_args->maxFile; //nilai nama maxFile disimpan di maxFile
    char ext[4]; //diisi dengan nilai dirname
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/"; //dideklarasi array karakter dengan ukuran lalu diinisialisasi striong categorized
    char new_dirname[MAX_LEN]; 
    struct stat st;//variabel yang menyimpan informasi file/direktori

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);

    //hitung banyak file
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r"); //mengeksekusi command yang disimpan dalam variabel cmd dan membuka stream untuk baca output
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // output dari kecil ke besar
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
    
    //menghitung total direktori yang dibuat per ext 
    int numDir; //menampung jumlah direktori yang dibuat
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);

    //create dir  if numDir == 0
    if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // jika direktori berhasil dibuat, maka kerjakan sesuatu
        writeLogMade(new_dirname);
    }

    for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //memindahkan file menurut extension nya
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];
                    
                    //untuk mencatat akses ke folder yang sedang diproses saat ini
                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    //mencatat akses ke folder tujuan
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    //untuk menentukan path tujuan pemindahan file dengan memindahkan file dengan ekstensi terntentu ke dalam forder
                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);

                    //untuk menambahkan pesan log "MOVED" ketila file dipindahkan
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
            }
    }
    return NULL;
}
```
- Kode tersebut merupakan implementasi dari suatu fungsi yang memiliki tujuan untuk membuat direktori baru secara otomatis berdasarkan jenis ekstensi file yang telah ditentukan sebelumnya.
- Fungsi ini menerima satu argumen dalam bentuk struktur data ```ThreadArgs``` yang berisi informasi mengenai nama direktori, batasan jumlah file dalam satu direktori, serta jumlah file yang memiliki ekstensi yang sama dengan nama direktori tersebut.

- Penjelasan fungsi ini secara detail adalah sebagai berikut :

**Pertama**
```c
struct ThreadArgs* thread_args = (struct ThreadArgs*) arg; //argumen arg dikonversi menjadi ponter ke struktur Thread args
    char* dirname = thread_args->dirname; //nilai nama direktori disimpan di dirname
    int maxFile = thread_args->maxFile; //nilai nama maxFile disimpan di maxFile
    char ext[4]; //diisi dengan nilai dirname
    strcpy(ext, dirname);
    char categorized_dirname[MAX_LEN] = "categorized/"; //dideklarasi array karakter dengan ukuran lalu diinisialisasi striong categorized
    char new_dirname[MAX_LEN]; 
    struct stat st;//variabel yang menyimpan informasi file/direktori

    // membuat nama direktori baru dengan menambahkan prefix "categorized/"
    strcpy(new_dirname, categorized_dirname);
    strcat(new_dirname, dirname);
```

Kode tersebut merupakan implementasi dari fungsi ```create_directory``` yang akan dieksekusi oleh sebuah thread untuk membuat sebuah direktori baru. Fungsi ini menerima satu argumen berupa argumen pointer yang mengacu pada struktur data ```ThreadArgs``` yang berisi informasi mengenai nama direktori dan nilai ```maxFile```.

**Kedua**
```c
    char countFile[256];
    char hehe_dir[20] ="files";
    char cmd[256];
    snprintf(cmd, sizeof(cmd), "find %s -type f -iname \"*.%s\" | wc -l", hehe_dir, ext);

    // Mengeksekusi command dan membuka stream untuk membaca outputnya
    FILE* fp;
    fp = popen(cmd, "r"); //mengeksekusi command yang disimpan dalam variabel cmd dan membuka stream untuk baca output
    if (fp == NULL) {
        printf("Error: Failed to execute command.\n");
    }

    // Membaca output dari stream dan menampilkannya ke layar
    writeLogAccess(hehe_dir);
    fgets(countFile, sizeof(countFile), fp);
    int numFiles = atoi(countFile);

    // output dari kecil ke besar
    char _write[MAX_LEN];
    system("touch buffer.txt");
    sprintf(_write, "echo \"extension_%s : %d\" >> buffer.txt", ext, numFiles);
    system(_write);
```
Kode tersebut digunakan untuk menghitung jumlah file yang ada di direktori ```files``` dengan ekstensi yang telah ditentukan sebelumnya.

**Ketiga**
```c
int numDir; //menampung jumlah direktori yang dibuat
    if (maxFile == 0){
        numDir = 1;
    } else {
        numDir = numFiles / maxFile ;
        if (numFiles % maxFile > 0)
            numDir = numDir + 1;
    }

    // Menutup stream dan mengembalikan nilai 0 (sukses)
    pclose(fp);
```
Kode tersebut berfungsi untuk menghitung jumlah direktori yang telah dibuat berdasarkan jumlah file yang ada dalam satu jenis ekstensi tertentu dan batasan maksimum file dalam satu direktori.

**Keempat**
```c
if (numDir == 0 && mkdir(new_dirname, 0777) == 0) {
    // jika direktori berhasil dibuat, maka kerjakan sesuatu
        writeLogMade(new_dirname);
    }
```
- Kode tersebut digunakan untuk membuat direktori baru. Jika nilai variabel ```numDir``` sama dengan 0, maka tidak perlu membuat subdirektori baru karena tidak ada file yang akan disimpan di dalamnya. Selanjutnya, program akan membuat direktori baru dengan menggunakan fungsi ```mkdir()```, dengan menggunakan parameter ```new_dirname``` sebagai nama direktori baru.

- Apabila pembuatan direktori berhasil dilakukan (nilai yang dikembalikan oleh ```mkdir()```sama dengan 0), maka fungsi ```writeLogMade()``` akan dipanggil dengan menggunakan parameter nama ```new_dirname```. Fungsi ini berfungsi untuk mencatat log atau riwayat pembuatan direktori pada file ```log.txt ``` yang telah ditentukan sebelumnya.

**Kelima**
```c
for (int i = 0; i < numDir; i++)
    {
         // mendefinisikan variable nama directory dengan suffix sesuai dengan index
         char _dirName[30];
         strcpy(_dirName, new_dirname);
         if(i > 0) {
            char suffix[10];
            sprintf(suffix, " (%d)", i+1);
            strcat(_dirName, suffix);
        }       

        // memeriksa apakah direktori sudah ada
        if (stat(_dirName, &st) == 0) {
            printf("Thread %ld: Direktori '%s' sudah ada.\n", pthread_self(), _dirName);
        } else {
                // membuat direktori jika belum ada
                if (mkdir(_dirName, 0777) == 0) {
                    writeLogMade(_dirName);
                    //memindahkan file menurut extension nya
                    char command1[600];
                    char command2[500];
                    char command3[100];
                    char command4[600];
                    char logAccessSource[200];
                    char logAccessTarget[100];
                    
                    //untuk mencatat akses ke folder yang sedang diproses saat ini
                    sprintf(logAccessSource, "find %s/ -type f -iname \"*.%s\" |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt", hehe_dir, ext);
                    //mencatat akses ke folder tujuan
                    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", _dirName);
                    
                    system (logAccessSource);
                    system (logAccessTarget);

                    //untuk menentukan path tujuan pemindahan file dengan memindahkan file dengan ekstensi terntentu ke dalam forder
                    sprintf(command3, "mv \"{}\" \"%s/\"", _dirName);

                    //untuk menambahkan pesan log "MOVED" ketila file dipindahkan
                    sprintf(command2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED %s file : {} > %s\">> log.txt && %s", ext, _dirName, command3);

                    if (maxFile == 0)
                    {
                        sprintf(command4, "find %s/ -type f -iname \"*.%s\" |xargs -I {} sh -c '%s' ", hehe_dir, ext, command2);
                        system(command4);
                    } else {
                        sprintf(command1, "find %s/ -type f -iname \"*.%s\" |head -%d |xargs -I {} sh -c '%s' ", hehe_dir, ext, maxFile, command2);

                        system(command1);

                    }


                } else {
                    perror("Gagal membuat direktori");
                    
                }
```
Kode ini adalah bagian dari program yang berfungsi untuk memindahkan file dengan ekstensi tertentu ke beberapa direktori sesuai dengan jumlah maksimum file dalam satu direktori yang ditentukan oleh pengguna. Kode ini akan melakukan perulangan sebanyak ```numDir``` kali untuk membuat direktori-direktori tersebut.

Pada fungsi main terdapat kode di bawah ini:
```c
    log_file = fopen("log.txt", "a");
    if (log_file == NULL) {
        perror("Failed to open log file");
        exit(1);
    }

    // inisialisasi mutex lock
    if (pthread_mutex_init(&lock, NULL) != 0) {
        perror("Failed to initialize mutex lock");
        exit(1);
    }
```
- Kode di atas merupakan inisialisasi awal yang dilakukan pada program. 
- Pertama, menggunakan fungsi ```fopen()``` untuk membuka file ```log.txt``` dengan mode ```a``` yang menambahkan isi file baru pada akhir file yang sudah ada. Ini bertujuan agar log dari setiap proses yang dilakukan akan ditambahkan pada akhir file ```log.txt``` yang sudah ada. Namun jika pembukaan file gagal, program akan keluar dan menampilkan pesan error.

- Kedua, menggunakan fungsi ```pthread_mutex_init()``` untuk menginisialisasi mutex lock pada program. Mutex lock berfungsi untuk mengatur akses ke bagian program yang sama agar tidak terjadi akses secara bersamaan oleh beberapa thread pada saat yang sama. Jika inisialisasi gagal, program akan keluar dan menampilkan pesan error.

```c
 if (stat("categorized", &st) != 0) {
        mkdir("categorized", 0777);
        writeLogMade("categorized");
    }
```
Kode di atas akan melakukan pengecekan apakah direktori ```categorized``` sudah ada atau belum. Jika ternyata direktori tersebut belum ada, maka kode akan membuat direktori baru dengan menggunakan fungsi ```mkdir()```. Selain itu, kode juga akan menuliskan pesan log menggunakan fungsi ```writeLogMade()``` yang bertugas mencatat bahwa direktori ```categorized``` telah berhasil dibuat.

```c
    char maxfilename[50] = "max.txt";
    //open max.txt
    FILE *max;
    // membuka file untuk dibaca
    max = fopen(maxfilename, "r");

    // memeriksa apakah file berhasil dibuka
    if (max == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }

    int maxFile;
    fscanf(max, "%d", &maxFile); // untuk ngebaca line pertama sampai habis

     // menutup file max.txt
    fclose(max);
```
Dalam program ini, ada variabel bernama ```maxfilename``` yang berisi string ```max.txt``` yang merupakan nama file yang ingin dibuka. Menggunakan fungsi ```fopen()``` dengan mode ```r```, program membuka file tersebut dan menyimpan pointer ke dalam variabel ```max```. Program kemudian membaca nilai integer pertama di dalam file ```max.txt``` menggunakan fungsi ```fscanf()``` dan menyimpannya dalam variabel ```maxFile```. Nilai ini akan digunakan untuk menghitung jumlah direktori yang akan dibuat di program selanjutnya. Setelah selesai membaca file, program menutup file tersebut dengan menggunakan fungsi ```fclose()```. Jika file tidak dapat dibuka, maka program akan menampilkan pesan "File tidak dapat dibuka" dan keluar dari program menggunakan fungsi ```exit(1)```.

```c
char filename[50] = "extensions.txt";
    char dirname[MAX_LEN];
    FILE *fp;
    struct stat st;
    pthread_t tid[MAX_LEN];
    struct ThreadArgs thread_args[MAX_LEN];
    int i = 0;

     // membuka file extensions.txt untuk dibaca
    fp = fopen(filename, "r");

    // memeriksa apakah file extensions.txt berhasil dibuka
    if (fp == NULL) {
        printf("File tidak dapat dibuka.\n");
        exit(1);
    }
```
Kode di atas berguna untuk membaca file ```extensions.txt``` yang berisi daftar ekstesi file yang akan dikategorikan

```c
    while (fgets(dirname, MAX_LEN, fp)) {
        // menghilangkan karakter newline di akhir string
        dirname[strcspn(dirname, "\n")] = 0;

        // menghapus spasi di akhir string
        size_t len = strlen(dirname);
        while (len > 0 && isspace(dirname[len - 1])) {
            len--;
        }
        dirname[len] = '\0';

        // membuat direktori lain secara asynchronous dengan menggunakan thread
        strcpy(thread_args[i].dirname, dirname);
        thread_args[i].maxFile = maxFile;
        pthread_create(&tid[i], NULL, create_directory, &thread_args[i]);
        i++;
    }
```
Kode di atas merupakan program untuk membaca nama direktori dari file ```extensions.txt```. Kode ini juga menggunakan thread untuk membuat direktori baru secara asynchronous

```c
 for (int j = 0; j < i; j++) {
        pthread_join(tid[j], NULL);
    }
```
Kode ini menggunakan loop untuk menjalankan fungsi ```pthread_join()``` yang berguna untuk menggabungkan thread-thread yang masih berjalan ke dalam thread utama.

```c
    if (stat("categorized/other", &st) != 0) {
        writeLogMade("categorized/other");
        if (mkdir("categorized/other", 0777) != 0)
            printf("Gagal membuat direktori 'other' di dalam 'categorized'.\n");
    }
```
Kode ini berguna untuk membuat direktori ```other``` di dalam direktori ```categorized``` dika direktori tersebut belum ada

```c
    char cmdOther1[600];
    char cmdOther2[500];
    char cmdOther3[100];
    char otherPath[20] = "categorized/other";
    char logAccessSource[200];
    char logAccessTarget[100];

    sprintf(logAccessSource, "find files/ -type f |xargs -I{} sh -c 'echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname \"{}\")\"' >> log.txt");
    sprintf(logAccessTarget, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") ACCESSED $(dirname '%s') \">> log.txt", otherPath);
    
    system(logAccessSource);
    system(logAccessTarget);
    //melakukan pemindahan file yang tidak memiliki ekstensi yang terdaftar ke dalam folder other dan mencatat setiap file dipindahkan dalam log
    sprintf(cmdOther3, "mv '\\''{}'\\'' \"%s/\"", otherPath);
    sprintf(cmdOther2, "echo \"$(date +\"%%d-%%m-%%Y %%H:%%M:%%S\") MOVED other file : '\\''{}'\\'' > %s\">> log.txt && %s", otherPath, cmdOther3);
    sprintf(cmdOther1, "find files/ -type f |xargs -I {} sh -c '%s' ", cmdOther2);
    system(cmdOther1);
```
Kode di atas bertujuan untuk mengkategorikan file-file yang terdapat pada direktori ```files``` berdasarkan ekstensi file tersebut. Kode ini akan memindahkan file-file yang tidak memiliki ekstensi yang terdaftar ke dalam direktori ```categorized/other```.

```c
    system("sort buffer.txt");
    system("rm -rf buffer.txt");
```
Kode ini menjalankan dua perintah pada terminal, yaitu ```sort``` dan ```rm```. Pertama, perintah ```sort``` digunakan untuk mengurutkan isi dari file ```buffer.txt```. Setelah itu, perintah ```rm``` digunakan untuk menghapus file ```buffer.txt```. Oleh karena itu, setelah menjalankan kode ini, akan ditampilkan output yang menunjukkan jumlah file untuk setiap ekstensi, dengan format yang diminta, dan diurutkan secara ascending.

```c
 DIR *dir;
    struct dirent *ent;
    int count = 0;
    if ((dir = opendir (otherPath)) != NULL) {
        writeLogAccess(otherPath);
        while ((ent = readdir (dir)) != NULL) {
            if(ent->d_type == DT_REG) {
                count++;
            }
        }
        closedir (dir);
    } else {
        perror ("Tidak bisa membuka direktori");
        return 1;
    }
    printf("other : %d\n", count);

    // menutup file
    fclose(fp);
    
   // menghancurkan mutex lock
    pthread_mutex_destroy(&lock);

    // menutup file log
    fclose(log_file);
```
Kode di atas berguna untuk menghitung file yang terdapat di direktori ```categorized/other```

### Pembahasan File logchecker.c 

```c
printf ("\e[33mTotal banyak akses:\e[0m\n");
system("grep ACCESSED log.txt | wc -l");
```
Program ini berfungsi untuk menghitung jumlah total baris dalam file ```log.txt``` yang mengandung string ```ACCESSED```. 
- Pertama, menggunakan perintah ```printf()``` untuk menampilkan teks ```Total banyak akses:``` dengan warna kuning (ditandai dengan kode ```\e[33m dan \e[0m)```.
- Selanjutnya, menggunakan perintah ```system()``` untuk mengeksekusi perintah shell ```grep ACCESSED log.txt | wc -l```. Perintah ```grep``` akan mencari semua baris dalam file ```log.txt``` yang mengandung string ```ACCESSED```, dan perintah ```wc``` akan menghitung jumlah baris yang ditemukan. Hasil dari perintah tersebut akan ditampilkan pada terminal sebagai output dari program.


```c
    printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");
    system("grep 'MOVED\\|MADE' log.txt \
    | awk -F 'categorized' '{print \"categorized\"$NF}' \ 
    | sed '/^$/d' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \"$3 \"=\" \" \" $1-1}'");
```
Kode di atas berguna untuk menampilkan list folder dan jumlah total file pada tiap folder setelah proses pemindahan file selesai.

Note :

- ```printf ("\e[33m\nList folder & total file tiap folder:\e[0m\n");``` => menampilkan pesan "List folder & total file tiap folder" 

- ```system("grep 'MOVED\|MADE' log.txt")``` => untuk mencari dan menampilkan baris yang mengandung kata "MOVED" atau "MADE" pada file ```log.txt```.

- ```awk -F 'categorized' '{print "categorized"$NF}'``` => untuk memisahkan baris yang berisi path file yang sudah dipindahkan ke dalam folder "categorized" dan mengambil path tersebut sebagai output.

- ```sed '/^$/d'``` => untuk menghapus baris yang hanya berisi karakter new line.

- ```sort``` => untuk mengurutkan output secara ascending.

- ```uniq -c``` => untuk menghitung jumlah kemunculan dari tiap baris.

- ```awk '{print $2 " "$3 "=" " " $1-1}'``` => untuk memformat output dengan menampilkan path folder (dari kolom kedua), jumlah file pada folder tersebut (dari kolom ketiga), dan tanda sama dengan (=) di antara keduanya.

```c
 printf ("\n\e[33mTotal file tiap extension\e[0m\n");
    system("grep 'MADE\\|MOVED' log.txt \
    | grep -o 'categorized/[^o]*' \
    | sed 's/categorized\\///' \
    | sed '/^$/d' \
    | sed 's/ \\([^o]*\\)//' \
    | sort \
    | uniq -c \
    | awk '{print $2 \" \" \"=\" \" \" $1-1}'");
```
Kode yang dilakukan pada program ini mirip dengan kode sebelumnya. Hal ini dilakukan karena program memerlukan semua nama extension yang tersedia, yang dapat diambil dari log "MADE" dan "MOVED". Selanjutnya, untuk memperoleh hanya extension-nya saja, dapat diambil dari kata kunci setelah ```categorized/```. Untuk menghilangkan direktori dengan suffix numbering, dapat digunakan fungsi ```sed```. 

Note :
Untuk keggunaan perintah pada shell akan sama saja dengan yang sebelumnya







