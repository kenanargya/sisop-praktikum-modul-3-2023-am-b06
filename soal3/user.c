#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/sem.h>

#define MSG_SIZE 2048

struct message
{
  long type;
  char text[1024];
};

union semun
{
  int val;
  struct semid_ds *buf;
  unsigned short *array;
};

int main()
{
  int msgid;
  key_t key = ftok("stream.c", 'A');
  if (key == -1)
  {
    perror("ftok");
    exit(1);
  }
  msgid = msgget(key, 0666 | IPC_CREAT);
  if (msgid == -1)
  {
    perror("msgget");
    exit(1);
  }

  int semid;
  union semun arg;
  struct sembuf sb = {0, -1, 0}; // Wait operation
  key_t semkey = ftok("user.c", 'B');
  if (semkey == -1)
  {
    perror("ftok");
    exit(1);
  }
  semid = semget(semkey, 1, IPC_CREAT | 0666);
  if (semid == -1)
  {
    perror("semget");
    exit(1);
  }
  arg.val = 2; // Set semaphore value to 2 (number of allowed users)
  if (semctl(semid, 0, SETVAL, arg) == -1)
  {
    perror("semctl");
    exit(1);
  }

  char command[MSG_SIZE];
  
  pid_t pid = getpid();
  printf("User %d connected to stream.\n", pid);

  while (1)
  {
    // Wait for semaphore to become available
    struct sembuf sem_wait = {0, -1, 0};
    semop(semid, &sem_wait, 1);

    printf("Enter command: ");
    fgets(command, MSG_SIZE, stdin);
    strtok(command, "\n");

    struct message msg;
    msg.type = pid;

    if (strncmp(command, "DECRYPT", 7) == 0)
    {
      strcpy(msg.text, "DECRYPT");
    }
    else if (strncmp(command, "LIST", 4) == 0)
    {
      strcpy(msg.text, "LIST");
    }
    else if (strncmp(command, "PLAY", 4) == 0)
    {
      char *song = strtok(command + 5, "\"");
      if (song == NULL)
      {
        printf("Invalid command: %s\n", command);
        continue;
      }
      // Convert song to uppercase for case-insensitive comparison
      for (int i = 0; i < strlen(song); i++)
      {
        song[i] = toupper(song[i]);
      }
      sprintf(msg.text, "PLAY %s", song);
    }
    else if (strncmp(command, "ADD", 3) == 0)
    {
      char *song = strtok(command + 4, "\n");
      if (song == NULL)
      {
        printf("Invalid command: %s\n", command);
        continue;
      }
      sprintf(msg.text, "ADD %s", song);
    }
    else
    {
      printf("Unknown command: %s\n", command);
      continue;
    }

    if (msgsnd(msgid, &msg, sizeof(msg), 0) == -1)
    {
      perror("msgsnd");
      exit(1);
    }

    // Release semaphore
    struct sembuf sem_signal = {0, 1, 0};
    semop(semid, &sem_signal, 1);
  }

  // Remove message queue and semaphore
  msgctl(msgid, IPC_RMID, NULL);
  semctl(semid, 0, IPC_RMID, arg);

  return 0;
}
