#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <cjson/cJSON.h>
#include <sys/msg.h>
#include <errno.h>
#include <sys/sem.h>
#include <stdint.h>


#define MAX_MESSAGE_SIZE 2048
#define MAX_SONGS 1005
#define MAX_SONG_NAME_LENGTH 50

struct message
{
  long mtype;
  char mtext[MAX_MESSAGE_SIZE];
};


union semun {
  int val;
  struct semid_ds *buf;
  unsigned short int *array;
  struct seminfo *__buf;
};

// Rot13 decoding function
static char *rot13_decode(const char *input)
{
    int len = strlen(input);
    char *output = malloc(len + 1);
    if (output == NULL)
    {
        return NULL;
    }
    const char *input_ptr = input;
    char *output_ptr = output;
    while (*input_ptr)
    {
        char c = *input_ptr++;
        if (isalpha(c))
        {
            if (islower(c))
            {
                c = 'a' + ((c - 'a' + 13) % 26);
            }
            else
            {
                c = 'A' + ((c - 'A' + 13) % 26);
            }
        }
        *output_ptr++ = c;
    }
    *output_ptr = '\0';
    return output;
}

static const unsigned char base64_table[256] = {
    ['A'] = 0, ['B'] = 1, ['C'] = 2, ['D'] = 3, ['E'] = 4, ['F'] = 5, ['G'] = 6, ['H'] = 7, ['I'] = 8, ['J'] = 9, ['K'] = 10, ['L'] = 11, ['M'] = 12, ['N'] = 13, ['O'] = 14, ['P'] = 15, ['Q'] = 16, ['R'] = 17, ['S'] = 18, ['T'] = 19, ['U'] = 20, ['V'] = 21, ['W'] = 22, ['X'] = 23, ['Y'] = 24, ['Z'] = 25, ['a'] = 26, ['b'] = 27, ['c'] = 28, ['d'] = 29, ['e'] = 30, ['f'] = 31, ['g'] = 32, ['h'] = 33, ['i'] = 34, ['j'] = 35, ['k'] = 36, ['l'] = 37, ['m'] = 38, ['n'] = 39, ['o'] = 40, ['p'] = 41, ['q'] = 42, ['r'] = 43, ['s'] = 44, ['t'] = 45, ['u'] = 46, ['v'] = 47, ['w'] = 48, ['x'] = 49, ['y'] = 50, ['z'] = 51, ['0'] = 52, ['1'] = 53, ['2'] = 54, ['3'] = 55, ['4'] = 56, ['5'] = 57, ['6'] = 58, ['7'] = 59, ['8'] = 60, ['9'] = 61, ['+'] = 62, ['/'] = 63};

// Base64 decoding function
static char *base64_decode(const char *input)
{
    int len = strlen(input);
    if (len % 4 != 0)
    {
        return NULL;
    }

    int output_len = 3 * (len / 4);
    if (input[len - 1] == '=')
    {
        output_len--;
    }
    if (input[len - 2] == '=')
    {
        output_len--;
    }

    char *output = (char *)malloc(output_len + 1);
    if (output == NULL)
    {
        return NULL;
    }

    for (int i = 0, j = 0; i < len;)
    {
        uint32_t sextet_a = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];
        uint32_t sextet_b = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];
        uint32_t sextet_c = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];
        uint32_t sextet_d = input[i] == '=' ? 0 & i++ : base64_table[input[i++]];

        uint32_t triple = (sextet_a << 3 * 6)
                        + (sextet_b << 2 * 6)
                        + (sextet_c << 1 * 6)
                        + (sextet_d << 0 * 6);

        if (j < output_len)
        {
            output[j++] = (triple >> 2 * 8) & 0xFF;
        }
        if (j < output_len)
        {
            output[j++] = (triple >> 1 * 8) & 0xFF;
        }
        if (j < output_len)
        {
            output[j++] = (triple >> 0 * 8) & 0xFF;
        }
    }

    output[output_len] = '\0';
    return output;
}


// Hex decoding function
static unsigned char* hex_decode(const char* input) {
  int len = strlen(input);
  if (len % 2 != 0) {
    return NULL;
  }

  int output_len = len / 2;
  char* output = malloc(output_len + 1);
  if (output == NULL) {
    return NULL;
  }

  for (int i = 0; i < len; i += 2) {
    char c1 = input[i];
    char c2 = input[i + 1];

    int value = 0;
    if (c1 >= '0' && c1 <= '9') {
      value += (c1 - '0') * 16;
    } else if (c1 >= 'a' && c1 <= 'f') {
      value += (c1 - 'a' + 10) * 16;
    } else if (c1 >= 'A' && c1 <= 'F') {
      value += (c1 - 'A' + 10) * 16;
    } else {
      free(output);
      return NULL;
    }

    if (c2 >= '0' && c2 <= '9') {
      value += c2 - '0';
    } else if (c2 >= 'a' && c2 <= 'f') {
      value += c2 - 'a' + 10;
    } else if (c2 >= 'A' && c2 <= 'F') {
      value += c2 - 'A' + 10;
    } else {
      free(output);
      return NULL;
    }

    output[i / 2] = (char)value;
  }

  output[output_len] = '\0';
  return output;
}

void add_playlist(char *song_name)
{
  // Open the playlist file for reading
  FILE *playlist = fopen("playlist.txt", "r");
  if (playlist == NULL)
  {
    printf("Error opening playlist file for reading\n");
    return 0;
  }

  // Check if song is already in the playlist
  char line[MAX_SONG_NAME_LENGTH];
  while (fgets(line, MAX_SONG_NAME_LENGTH, playlist) != NULL)
  {
    line[strcspn(line, "\n")] = '\0'; // Remove newline character
    if (strcasecmp(line, song_name) == 0)
    {
      printf("SONG ALREADY ON PLAYLIST\n");
      fclose(playlist);
      return 0;
    }
  }

 
  fclose(playlist);

  // Open the playlist file for appending
  playlist = fopen("playlist.txt", "a");
  if (playlist == NULL)
  {
    printf("Error opening playlist file for appending\n");
    return 0;
  }

  fprintf(playlist, "%s\n", song_name);

  fclose(playlist);

  return 1;
}





int main()
{
  const char *filename = "song-playlist.json";

  int msgid;
  key_t key = ftok("stream.c", 'A');
  msgid = msgget(key, 0666 | IPC_CREAT);
  if (msgid == -1)
  {
    perror("Error creating message queue");
    exit(1);
  }

  key_t sem_key = ftok("user.c", 'B');
  int sem_id = semget(sem_key, 1, IPC_CREAT | 0666);
  if (sem_id == -1)
  {
    perror("semget failed");
    exit(1);
  }

  // Initialize semaphore (user) to 2 

  union semun sem_union;
  sem_union.val = 2;
  if (semctl(sem_id, 0, SETVAL, sem_union) == -1) {
    perror("semctl failed");
    exit(EXIT_FAILURE);
  }
  int user_count;
  struct sembuf sembuf_array[1];

  while (1)
  {
    // Menerima user command dari mqueue
    struct message message;
    if (msgrcv(msgid, &message, sizeof(message), 0, 0) == -1)
    {
      perror("Error receiving message");
      exit(1);
    }


    sembuf_array[0].sem_num = 0;
    sembuf_array[0].sem_op = -1;
    sembuf_array[0].sem_flg = SEM_UNDO;
    if (semop(sem_id, sembuf_array, 1) == -1) {
      if (errno == EINTR) {
        continue;
      }
      perror("semop failed");
      exit(EXIT_FAILURE);
    }
    

    sem_union.val = 0;
    user_count = semctl(sem_id, 0, GETVAL, sem_union);
    if (user_count < 0) {
        perror("semctl failed");
        exit(EXIT_FAILURE);
    }
    if (user_count < 2) {
        sembuf_array[0].sem_op = 1;
        if (semop(sem_id, sembuf_array, 1) == -1) {
            if (errno == EINTR) {
            continue;
            }
            perror("semop failed");
            exit(EXIT_FAILURE);
        }
    } else {
        printf("STREAM SYSTEM OVERLOAD\n");
        continue;
    }

    // parsing command
    char *command = message.mtext;
    char *param = strchr(command, ' ');
    if (param)
    {
      *param = '\0';
      param++;
      while (isspace(*param))
      {
        param++;
      }
    }

    if (strcmp(command, "DECRYPT") == 0)
    {
      FILE *fp = fopen(filename, "r");
      if (fp == NULL)
      {
        printf("Error opening file: %s\n", filename);
        return 1;
      }
      fseek(fp, 0, SEEK_END);
      long file_size = ftell(fp);
      fseek(fp, 0, SEEK_SET);
      char *json_str = malloc(file_size + 1);
      if (json_str == NULL)
      {
        printf("Error allocating memory\n");
        return 1;
      }
      fread(json_str, 1, file_size, fp);
      fclose(fp);
      json_str[file_size] = '\0';

      cJSON *root = cJSON_Parse(json_str);
      if (root == NULL)
      {
        printf("Error parsing JSON: %s\n", cJSON_GetErrorPtr());
        free(json_str);
        return 1;
      }

      FILE *playlist = fopen("playlist.txt", "w");
      if (playlist == NULL)
      {
        printf("Error opening playlist file for writing\n");
        cJSON_Delete(root);
        free(json_str);
        return 1;
      }

      cJSON *item = NULL;
      cJSON_ArrayForEach(item, root)
      {
        cJSON *method = cJSON_GetObjectItemCaseSensitive(item, "method");
        cJSON *song = cJSON_GetObjectItemCaseSensitive(item, "song");

        if (strcmp(method->valuestring, "base64") == 0)
        {
          char *decoded = base64_decode(song->valuestring);
          if (decoded == NULL)
          {
            printf("Error decoding base64: %s\n", song->valuestring);
            continue;
          }
          
          fprintf(playlist, "%s\n", decoded);
          free(decoded);
        }
        else if (strcmp(method->valuestring, "hex") == 0)
        {
          char *decoded = hex_decode(song->valuestring);
          if (decoded == NULL)
          {
            printf("Error decoding hex: %s\n", song->valuestring);
            continue;
          }
          
          fprintf(playlist, "%s\n", decoded);
          free(decoded);
        }
        else if (strcmp(method->valuestring, "rot13") == 0)
        {
          char *decoded = rot13_decode(song->valuestring);
          if (decoded == NULL)
          {
            printf("Error decoding rot13: %s\n", song->valuestring);
            continue;
          }
          
          fprintf(playlist, "%s\n", decoded);
          free(decoded);
        }
        else
        {
          printf("Unknown method: %s\n", method->valuestring);
          continue;
        }
    }


    fclose(playlist);
    cJSON_Delete(root);
    free(json_str);

    system("sort playlist.txt > playlist_sorted.txt");
    system("cp playlist_sorted.txt playlist.txt");
    system("rm playlist_sorted.txt");

    }
    else if (strcmp(command, "LIST") == 0)
    {
      system("cat playlist.txt");
    }

    else if (strcmp(command, "PLAY") == 0)
    {
      // Search for songs containing the query
      char *query = param;
      int num_matches = 0;
      char matches[MAX_SONGS][MAX_SONG_NAME_LENGTH];
      FILE *playlist = fopen("playlist.txt", "r");
      if (playlist == NULL)
      {
        printf("Error opening playlist file for reading\n");
        return 1;
      }
      char song_name[MAX_SONG_NAME_LENGTH];
      while (fgets(song_name, MAX_SONG_NAME_LENGTH, playlist) != NULL)
      {
        // Remove trailing newline if present
        song_name[strcspn(song_name, "\n")] = '\0';

        // Check if song name contains the query
        char *found = strcasestr(song_name, query);
        if (found != NULL)
        {
          strcpy(matches[num_matches], song_name);
          num_matches++;
        }
      }
      fclose(playlist);

      // Print search results
      if (num_matches == 0)
      {
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", query);
      }
      else if (num_matches > 1)
      {
        printf("THERE ARE \"%d\" SONG(S) CONTAINING \"%s\":\n", num_matches, query);
        for (int i = 0; i < num_matches; i++)
        {
          printf("%d. %s\n", i + 1, matches[i]);
        }
      }
      else if (num_matches == 1)
      {
        // Play the first matching song
        printf("USER <%ld> PLAYING \"%s\"\n", message.mtype, matches[0]);
      }
    }  
    else if (strcmp(command, "ADD") == 0)
    {
      add_playlist(param);
      if(1) {
        printf("USER <%ld> ADD %s\n", message.mtype, param);
      }
        
    }

    // Release semaphore
    sembuf_array[0].sem_num = 0;
    sembuf_array[0].sem_op = 1;
    sembuf_array[0].sem_flg = SEM_UNDO;
    if (semop(sem_id, sembuf_array, 1) == -1) {
      if (errno == EINTR) {
        continue;
      }
      perror("semop failed");
      exit(EXIT_FAILURE);
    }

  }

//   if (msgctl(msgid, IPC_RMID, NULL) == -1) {
//       perror("Error removing message queue");
//       exit(1);
//   }

//   if (semctl(sem_id, 0, IPC_RMID) == -1) {
//     perror("Error removing semaphore");
//     exit(1);
//   }

  return 0;
}
