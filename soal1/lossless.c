#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

#define MAX_TREE_HT 100
#define ALF 26

struct MinHeap_Node {
	char data_node;
	unsigned freq_node;
	struct MinHeap_Node *left, *right;
};

struct MinHeap {
	unsigned size;
	unsigned capacity;
	struct MinHeap_Node** array;
};

struct MinHeap_Node* newNode(char data_node, unsigned freq_node)
{
	struct MinHeap_Node* tempo = (struct MinHeap_Node*)malloc(
		sizeof(struct MinHeap_Node));

	tempo->left = tempo->right = NULL;
	tempo->data_node = data_node;
	tempo->freq_node = freq_node;

	return tempo;
}

struct MinHeap* createMinHeap(unsigned capacity)
{
	struct MinHeap* minHeap = (struct MinHeap*)malloc(sizeof(struct MinHeap));

	minHeap->size = 0;
	minHeap->capacity = capacity;
	minHeap->array = (struct MinHeap_Node**)malloc(minHeap->capacity * sizeof(struct MinHeap_Node*));
	return minHeap;
}

void swapMinHeapNode(struct MinHeap_Node** a, struct MinHeap_Node** b)
{
	struct MinHeap_Node* t = *a;
	*a = *b;
	*b = t;
}

void minHeapify(struct MinHeap* minHeap, int idx)
{
	int smallest = idx;
	int left = 2 * idx + 1;
	int right = 2 * idx + 2;

	if(left < minHeap->size && (minHeap->array[left]->freq_node) < (minHeap->array[smallest]->freq_node))
		smallest = left;

	if(right < minHeap->size && (minHeap->array[right]->freq_node) < (minHeap->array[smallest]->freq_node))
		smallest = right;

	if(smallest != idx) {
		swapMinHeapNode(&minHeap->array[smallest], &minHeap->array[idx]);
		minHeapify(minHeap, smallest);
	}
}

int isSizeOne(struct MinHeap* minHeap)
{
	return (minHeap->size == 1);
}

struct MinHeap_Node* extractMin(struct MinHeap* minHeap)
{
	struct MinHeap_Node* temp = minHeap->array[0];
	minHeap->array[0] = minHeap->array[minHeap->size - 1];

	(minHeap->size)--;
	minHeapify(minHeap, 0);
	return temp;
}

void insertMinHeap(struct MinHeap* minHeap, struct MinHeap_Node* minHeapNode)
{
	(minHeap->size)++;
	int i = minHeap->size - 1;
	while (i && minHeapNode->freq_node < minHeap->array[(i - 1) / 2]->freq_node) {
		minHeap->array[i] = minHeap->array[(i - 1) / 2];
		i = (i - 1) / 2;
	}
	minHeap->array[i] = minHeapNode;
}

void buildMinHeap(struct MinHeap* minHeap)
{
	int n = minHeap->size - 1;
	int i;
	for (i = (n - 1) / 2; i >= 0; --i) minHeapify(minHeap, i);
}

int isLeaf(struct MinHeap_Node* root)
{
	return !(root->left) && !(root->right);
}

struct MinHeap* createAndBuildMinHeap(char data_node[], int freq_node[], int size)
{
	struct MinHeap* minHeap = createMinHeap(size);

	for (int i = 0; i < size; ++i) minHeap->array[i] = newNode(data_node[i], freq_node[i]);
	minHeap->size = size;
	buildMinHeap(minHeap);
	return minHeap;
}

struct MinHeap_Node* buildHuffmanTree(char data_node[], int freq_node[], int size)
{
	struct MinHeap_Node *left, *right, *top;
	struct MinHeap* minHeap = createAndBuildMinHeap(data_node, freq_node, size);

	while (!isSizeOne(minHeap)) {
		left = extractMin(minHeap);
		right = extractMin(minHeap);
		top = newNode('$', left->freq_node + right->freq_node);
		top->left = left;
		top->right = right;
		insertMinHeap(minHeap, top);
	}
	return extractMin(minHeap);
}

void storeArr(int arr[], int n, int code[])
{
	int i;
	for (i=0; i<n; i++) code[i] = arr[i];
}

void printCodes(struct MinHeap_Node* root, int arr[], int top, int codes[][MAX_TREE_HT])
{
	// Assign 0 ke node kiri lalu rekursi
	if (root->left) {
		arr[top] = 0;
		printCodes(root->left, arr, top + 1, codes);
	}

	// Assign 1 to node kanan lalu rekursi
	if (root->right) {
		arr[top] = 1;
		printCodes(root->right, arr, top + 1, codes);
	}

	if (isLeaf(root)) {
		storeArr(arr, top, codes[root->data_node]);

	}
}

void HuffmanCodes(char data_node[], int freq_node[], int size, int codes[][MAX_TREE_HT])
{
	struct MinHeap_Node* root = buildHuffmanTree(data_node, freq_node, size);
	int arr[MAX_TREE_HT], top = 0;
	printCodes(root, arr, top, codes);
}

void sort_by_freq(char arr[], int freq_node[], int n) {
    char temp;
    for (int i=0; i<(n-1); i++) {
        for (int j=0; j<(n-i-1); j++) {
            if (freq_node[j] > freq_node[j+1]) {
                int tempFreq = freq_node[j];
                freq_node[j] = freq_node[j+1];
                freq_node[j+1] = tempFreq;

                temp = arr[j];
                arr[j] = arr[j+1];
                arr[j+1] = temp;
            }
        }
    }
}

int main()
{
	char arr[ALF] = { 'A', 'B', 'C', 'D', 'E', 'F',
                   	  'G', 'H', 'I', 'J', 'K', 'L',
                   	  'M', 'N', 'O', 'P', 'Q', 'R',
                   	  'S', 'T', 'U', 'V', 'W', 'X',
                   	  'Y', 'Z'};
	int freq[ALF] = {0};

   	char *filename = "file.txt";
    FILE *fp = fopen(filename, "r");

    char ch;
    char copy[1000]="0";
    int i=0;
    while ((ch = fgetc(fp)) != EOF){
		if(ch>=65 && ch<=90 || ch>=97 && ch<=122){
			copy[i]=ch;
        	i++;
		}
    }
    fclose(fp);

    for(int i=0; i<strlen(copy); i++){
        if(copy[i]>=97 && copy[i]<=122){
            copy[i]-=32;
        }
    }
	

	int fd1[2];
	int fd2[2];	

	if (pipe(fd1)==-1) 
	{ 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 
	if (pipe(fd2)==-1) 
	{ 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 

	int cid;
	cid=fork();
	if(cid==-1){
		printf("error saat fork proses\n");
	}else if(cid>0){	//parent
		close(fd1[0]);
  
    	for(int i=0; i<strlen(copy); i++){
        	for(int j=0; j<ALF; j++){
            	if(copy[i]==arr[j]){
                	freq[j]++;
                	break;
            	}
        	}
    	}

    	sort_by_freq(arr, freq, ALF);

    	for (int i = 0; i < ALF; i++) {
        	printf("%c ", arr[i]);
    	}
    	printf("\n");
    	for (int i = 0; i < ALF; i++) {
        	printf("%d ", freq[i]);
    	}
    	printf("\n");

		char buffer[100];	
		int con=0;

		for (int i=0; i<sizeof(arr); i++) {
        	buffer[con++] = arr[i];
    	}
    	for (int i=0; i<sizeof(freq)/sizeof(freq[0]); i++) {
        	buffer[con++] = freq[i];
    	}

		write(fd1[1], buffer, con);
		close(fd1[1]);
		wait(NULL); 

		printf("pipe 2 telah berhasil\n");

		close(fd2[1]);
		char Compreceive[4096];

		read(fd2[0], Compreceive, sizeof(Compreceive));
		close(fd2[0]);
		printf("\nencoded: \n%s\n", Compreceive);
		printf("\ndecoded: \n%s\n", copy);

		printf("\nTotal bit SEBELUM dilakukan kompresi huffman : %lu\n", strlen(copy)*8);
		printf("Total bit SETELAH dilakukan kompresi huffman : %lu\n", strlen(Compreceive));

	}else if(cid==0){	///child
		close(fd1[1]);
		char bufferc[100];
		char arrc[ALF];
		int freqc[ALF];
		int conc=0;

		read(fd1[0], bufferc, sizeof(bufferc));
		close(fd1[0]);

		for (int i=0; i<sizeof(arrc); i++) {
        	arrc[i]=bufferc[conc++];
    	}
    	for (int i=0; i<sizeof(freqc)/sizeof(freqc[0]); i++) {
        	freqc[i]=bufferc[conc++];
    	}
		printf("berhasil pipe fd1 dan ekstrak arr dan freq dari buffer\n");

		int codes[256][MAX_TREE_HT];
		for (int i=0; i<256; ++i) {
			for (int j = 0; j < MAX_TREE_HT; ++j) {
				codes[i][j] = -1;
			}
		}

		HuffmanCodes(arrc, freqc, ALF, codes);
		printf("-----------------------------------------------------------------------------------------\n");

		for (int i = 0; i < ALF; ++i) {
			printf("%c: ", arrc[i]);
			for (int j = 0; codes[arrc[i]][j] != -1; ++j) {
				printf("%d", codes[arrc[i]][j]);
			}
			printf("\n");
		}

		char comp[4096]="";
		int cnt=0;
		for(int i=0; i<strlen(copy); i++){
			for(int j=0; j<ALF; j++){

				if(copy[i]==arrc[j]){
					for (int l = 0; codes[arrc[j]][l] != -1; l++) {
						sprintf(comp+strlen(comp), "%d", codes[arrc[j]][l]); //concat string yang telah dienkode/ kompresi
					}
				}
			}
		}

		close(fd2[0]);
		write(fd2[1], comp, sizeof(comp));
		close(fd2[1]);
	}	

	return 0;
}
